//
// Created by AndreiMalcoci on 7/20/17.
//

#include <iostream>
#include <math.h>
#include "Fixed.hpp"

const int Fixed::fraction_bits = 8;

Fixed::Fixed() {
    std::cout << "Default constructor called" << std::endl;
    this->fixed_point = 0;
}

Fixed::Fixed(int const val)
{
    std::cout << "Int constructor called" << std::endl;
    this->fixed_point = val << fraction_bits;
}

Fixed::Fixed(float const val)
{
    std::cout << "Float constructor called" << std::endl;
    this->fixed_point =(int)roundf(val * (1 << fraction_bits));
}

Fixed::~Fixed() {
    std::cout << "Destructor called" << std::endl;
}

Fixed& Fixed::operator=(Fixed const &f) {
    std::cout << "Assignation operator called" << std::endl;
    this->fixed_point = f.fixed_point;
    return *this;
}

Fixed::Fixed(Fixed const &f)
{
    std::cout << "Copy constructor called" << std::endl;
    *this=f;
}

int Fixed::getRawBits() const
{
    std::cout << "getRawBits member function called" << std::endl;
    return this->fixed_point;
}

void Fixed::setRawBits(int const raw)
{
    this->fixed_point = raw;
}

int Fixed::toInt() const
{
    return (fixed_point >> fraction_bits);
}

float Fixed::toFloat() const {
    return (float) fixed_point / (1 << fraction_bits);
}

std::ostream& operator<<(std::ostream& out, const Fixed& f)
{
    out << f.toFloat();
    return out;
}