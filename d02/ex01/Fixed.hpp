//
// Created by AndreiMalcoci on 7/20/17.
//

#ifndef CPP_FIXED_HPP
#define CPP_FIXED_HPP


#include <ostream>

class Fixed {
private:
    int fixed_point;
    static const int fraction_bits;
public:
    Fixed();
    Fixed(int const val);
    Fixed(float const val);
    ~Fixed();
    Fixed(Fixed const &f);
    Fixed &operator=(Fixed const &f);
    int getRawBits( void ) const;
    void setRawBits( int const raw );
    float toFloat(void) const;
    int toInt(void) const;
};

std::ostream& operator<<(std::ostream& os, const Fixed& dt);

#endif //CPP_FIXED_HPP
