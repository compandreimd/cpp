//
// Created by AndreiMalcoci on 7/20/17.
//

#ifndef CPP_FIXED_HPP
#define CPP_FIXED_HPP


class Fixed {
private:
    int fixed_point;
    static const int fraction_bits;
public:
    Fixed();
    ~Fixed();
    Fixed(Fixed &f);
    void operator=(Fixed &f);
    int getRawBits( void ) const;
    void setRawBits( int const raw );
};

#endif //CPP_FIXED_HPP
