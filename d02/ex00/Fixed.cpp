//
// Created by AndreiMalcoci on 7/20/17.
//

#include <iostream>
#include "Fixed.hpp"

const int Fixed::fraction_bits = 8;

Fixed::Fixed() {
    std::cout << "Default constructor called" << std::endl;
    this->fixed_point = 0;
}

Fixed::~Fixed() {
    std::cout << "Destructor called" << std::endl;
}

void Fixed::operator=(Fixed &f) {
    std::cout << "Assignation operator called" << std::endl;
    this->fixed_point = f.getRawBits();
}

Fixed::Fixed(Fixed &f)
{
    std::cout << "Copy constructor called" << std::endl;
    *this=f;
}

int Fixed::getRawBits() const
{
    std::cout << "getRawBits member function called" << std::endl;
    return this->fixed_point;
}

void Fixed::setRawBits(int const raw)
{
    this->fixed_point = raw;
}