//
// Created by AndreiMalcoci on 7/20/17.
//

#include <iostream>
#include <math.h>
#include "Fixed.hpp"

const int Fixed::fraction_bits = 8;

Fixed::Fixed()
{
	this->fixed_point = 0;
}

Fixed::Fixed(int const val)
{
	this->fixed_point = val << fraction_bits;
}

Fixed::Fixed(float const val)
{
	this->fixed_point = (int)roundf(val * (1 << fraction_bits));
}

Fixed::~Fixed()
{
}

Fixed &Fixed::operator=(Fixed const &f)
{
	this->fixed_point = f.fixed_point;
	return *this;
}

Fixed::Fixed(Fixed const &f)
{
	*this = f;
}

int Fixed::getRawBits() const
{
	return this->fixed_point;
}

void Fixed::setRawBits(int const raw)
{
	this->fixed_point = raw;
}

int Fixed::toInt() const
{
	return (fixed_point >> fraction_bits);
}

float Fixed::toFloat() const
{
	return (float)fixed_point / (1 << fraction_bits);
}

std::ostream &operator<<(std::ostream &out, const Fixed &f)
{
	out << f.toFloat();
	return out;
}

bool Fixed::operator>(Fixed const &f) const
{
	return fixed_point > f.fixed_point;
}

bool Fixed::operator>=(Fixed const &f) const
{
	return fixed_point >= f.fixed_point;
}

bool Fixed::operator<(Fixed const &f) const
{
	return fixed_point < f.fixed_point;
}

bool Fixed::operator<=(Fixed const &f) const
{
	return fixed_point <= f.fixed_point;
}

bool Fixed::operator!=(Fixed const &f) const
{
	return fixed_point != f.fixed_point;
}

bool Fixed::operator==(Fixed const &f) const
{
	return fixed_point == f.fixed_point;
}

Fixed Fixed::operator+(Fixed rhs)
{
	Fixed f;

	f.setRawBits(this->fixed_point + rhs.fixed_point);
	return (f);
}

Fixed Fixed::operator-(Fixed rhs)
{
	Fixed f;

	f.setRawBits(this->fixed_point - rhs.fixed_point);
	return (f);
}

Fixed Fixed::operator*(Fixed rhs)
{
	Fixed f(this->toFloat() * rhs.toFloat());

	return (f);
}

Fixed Fixed::operator/(Fixed rhs)
{
	Fixed f(this->toFloat() / rhs.toFloat());

	return (f);
}

Fixed &Fixed::operator++()
{
	++fixed_point;
	return *this;
}

Fixed Fixed::operator++(int n)
{
	Fixed result(*this);
	++(*this);
	return (result);
}

Fixed &Fixed::operator--()
{
	--fixed_point;
	return *this;
}

Fixed Fixed::operator--(int n)
{
	Fixed result(*this);
	--(*this);
	return (result);
}


Fixed &Fixed::min(Fixed &lhs, Fixed &rhs)
{
	if (lhs < rhs)
		return (lhs);
	else
		return (rhs);
}

Fixed const &Fixed::min(Fixed const &lhs, Fixed const &rhs)
{
	if (lhs < rhs)
		return (lhs);
	else
		return (rhs);
}

Fixed &Fixed::max(Fixed &lhs, Fixed &rhs)
{
	if (lhs > rhs)
		return (lhs);
	else
		return (rhs);
}

Fixed const &Fixed::max(Fixed const &lhs, Fixed const &rhs)
{
	if (lhs > rhs)
		return (lhs);
	else
		return (rhs);
}
