//
// Created by AndreiMalcoci on 7/20/17.
//

#ifndef CPP_FIXED_HPP
#define CPP_FIXED_HPP


#include <ostream>

class Fixed
{
private:
	int fixed_point;
	static const int fraction_bits;
public:
	Fixed();

	Fixed(int const val);

	Fixed(float const val);

	~Fixed();

	Fixed(Fixed const &f);

	Fixed &operator=(Fixed const &f);

	int getRawBits(void) const;

	void setRawBits(int const raw);

	float toFloat(void) const;

	int toInt(void) const;

	bool operator>(Fixed const &f) const;

	bool operator>=(Fixed const &f) const;

	bool operator<(Fixed const &f) const;

	bool operator<=(Fixed const &f) const;

	bool operator!=(Fixed const &f) const;

	bool operator==(Fixed const &f) const;

	Fixed operator+(Fixed rhs);

	Fixed operator-(Fixed rhs);

	Fixed operator*(Fixed rhs);

	Fixed operator/(Fixed rhs);

	Fixed &operator++();

	Fixed operator++(int n);

	Fixed &operator--();

	Fixed operator--(int n);

	static Fixed &min(Fixed &lhs, Fixed &rhs);

	static Fixed const &min(Fixed const &lhs, Fixed const &rhs);

	static Fixed &max(Fixed &lhs, Fixed &rhs);

	static Fixed const &max(Fixed const &lhs, Fixed const &rhs);
};

std::ostream &operator<<(std::ostream &os, const Fixed &dt);

#endif //CPP_FIXED_HPP
