//
// Created by AndreiMalcoci on 7/26/17.
//

#ifndef CPP_CLAPTRAP_HPP
#define CPP_CLAPTRAP_HPP


#include <string>

class ClapTrap {
public:
    int hit_points;
    int max_hit_points;
    int energy_points;
    int max_energy_points;
    int lvl;
    std::string name;
    int meel_attack;
    int ranged_attack;
    int armor_damage;
    ClapTrap();
    ClapTrap(std::string name);
    ClapTrap(ClapTrap const &clapTrap);
    ClapTrap &operator=(ClapTrap const &clapTrap);
    ~ClapTrap();
    void rangedAttack(std::string const & target) const;
    void meleeAttack(std::string const & target) const;
    void takeDamage(unsigned int amount);
    void beRepaired(unsigned int amount);
};


#endif //CPP_CLAPTRAP_HPP
