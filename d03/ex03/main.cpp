#include <cstdlib>
#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"

int main() {
    srand(time(nullptr));
    FrapTrap *frap = new FrapTrap("F");
    ScavTrap *scav = new ScavTrap("S");
    NinjaTrap *ninja = new NinjaTrap("N");
    NinjaTrap n("M");

    n.ninajaTrap((ClapTrap*) frap);
    n.ninajaTrap(frap);
    n.ninajaTrap((ClapTrap*) scav);
    n.ninajaTrap(scav);
    n.ninajaTrap((ClapTrap*) ninja);
    n.ninajaTrap(ninja);
    delete frap;
    delete scav;
    delete ninja;
}