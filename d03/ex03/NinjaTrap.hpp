//
// Created by AndreiMalcoci on 7/26/17.
//

#ifndef CPP_NINJATRAP_HPP
#define CPP_NINJATRAP_HPP

#include "FragTrap.hpp"
#include "ScavTrap.hpp"

class NinjaTrap: public ClapTrap
{
public:
    NinjaTrap();
    NinjaTrap(std::string name);
    NinjaTrap(NinjaTrap const &trap);
    NinjaTrap &operator=(NinjaTrap const &trap);
    ~NinjaTrap();
    void ninajaTrap(ClapTrap *trap);
    void ninajaTrap(FrapTrap *trap);
    void ninajaTrap(ScavTrap *trap);
    void ninajaTrap(NinjaTrap *trap);
};


#endif //CPP_NINJATRAP_HPP
