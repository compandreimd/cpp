#include "FragTrap.hpp"

int main() {
    FragTrap a("A");
    FragTrap b(a);
    FragTrap *c = new FragTrap("C");
    delete (c);
    *c = b;
    a.takeDamage(10);
    a.beRepaired(120);
    a.takeDamage(123);
    a.meleeAttack("?");
    a.rangedAttack("?");
    a.vaulthunter_dot_exe("?");
    a.vaulthunter_dot_exe("?");
    a.vaulthunter_dot_exe("?");
    a.vaulthunter_dot_exe("?");
    a.vaulthunter_dot_exe("?");
    return 0;
}