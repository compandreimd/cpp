//
// Created by AndreiMalcoci on 7/23/17.
//

#ifndef CPP_FRAPTRAP_HPP
#define CPP_FRAPTRAP_HPP

#include <string>

class FragTrap
{
    int hit_points;
    int max_hit_points;
    int energy_points;
    int max_energy_points;
    int lvl;
    std::string name;
    int meel_attack;
    int ranged_attack;
    int armor_damage;
public:
    FragTrap();
    FragTrap(std::string name);
    FragTrap(FragTrap const &frapTrap);
    FragTrap &operator=(FragTrap const &frapTrap);
    ~FragTrap();
    void rangedAttack(std::string const & target) const;
    void meleeAttack(std::string const & target) const;
    void takeDamage(unsigned int amount);
    void beRepaired(unsigned int amount);
    void vaulthunter_dot_exe(std::string const & target);
};

#endif