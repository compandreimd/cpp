//
// Created by AndreiMalcoci on 7/26/17.
//

#ifndef CPP_SUPERTRAP_HPP
#define CPP_SUPERTRAP_HPP


#include "FragTrap.hpp"
#include "NinjaTrap.hpp"


class SuperTrap :public NinjaTrap,  public FragTrap {
public:
    SuperTrap(std::string const &name = "Hz");
    SuperTrap(SuperTrap const &st);
    SuperTrap&operator=(SuperTrap const &st);
    ~SuperTrap();
    using FragTrap::rangedAttack;
    using NinjaTrap::meleeAttack;
};


#endif //CPP_SUPERTRAP_HPP
