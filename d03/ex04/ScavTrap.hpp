//
// Created by AndreiMalcoci on 7/26/17.
//

#ifndef CPP_SCAVTRAP_HPP
#define CPP_SCAVTRAP_HPP


#include <string>
#include "ClapTrap.hpp"

class ScavTrap: public ClapTrap
{
public:
    ScavTrap();
    ScavTrap(std::string name);
    ScavTrap(ScavTrap const &scavTrap);
    ScavTrap &operator=(ScavTrap const &scavTrap);
    ~ScavTrap();
    void challengeNewcomer(std::string const &traget);
};


#endif //CPP_SCAVTRAP_HPP
