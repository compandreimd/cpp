//
// Created by AndreiMalcoci on 7/26/17.
//

#include <iostream>
#include "SuperTrap.hpp"

SuperTrap::SuperTrap(std::string const &name){
    this-> name = name;
    this->hit_points = 100;
    this->max_hit_points = 100;
    this->energy_points = 120;
    this->max_energy_points = 120;
    this->meel_attack = 60;
    this->ranged_attack = 5;
}

SuperTrap::SuperTrap(SuperTrap const &st) {
    this->FragTrap::name = st.FragTrap::name;
}

SuperTrap& SuperTrap::operator=(SuperTrap const &st) {
    this->FragTrap::name = st.FragTrap::name;
    return *this;
}

SuperTrap::~SuperTrap() {}