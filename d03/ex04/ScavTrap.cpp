//
// Created by AndreiMalcoci on 7/26/17.
//

#include "ScavTrap.hpp"

#include <iostream>
#include "FragTrap.hpp"


ScavTrap::ScavTrap() : ClapTrap() {
    this->energy_points = 50;
    this->max_energy_points = 50;
    this->meel_attack = 20;
    this->ranged_attack = 15;
    this->armor_damage = 3;
    std::cout << "SC4G-TP "<< this->name << " appears !" << std::endl;
}

ScavTrap::ScavTrap(std::string name) : ClapTrap(name){
    this->energy_points = 50;
    this->max_energy_points = 50;
    this->meel_attack = 20;
    this->ranged_attack = 15;
    this->armor_damage = 3;
    std::cout << "SC4G-TP "<< this->name << " appears !" << std::endl;
}

ScavTrap::ScavTrap(ScavTrap const &scavTrap)
{
    this->name = scavTrap.name;
    this->hit_points = scavTrap.hit_points;
    this->max_hit_points = scavTrap.max_hit_points;
    this->energy_points = scavTrap.energy_points;
    this->max_energy_points = scavTrap.max_energy_points;
    this->lvl = scavTrap.lvl;
    this->meel_attack = scavTrap.meel_attack;
    this->ranged_attack = scavTrap.ranged_attack;
    this->armor_damage = scavTrap.armor_damage;
    std::cout << "SC4G-TP "<< this->name << " cloned " << std::endl;
}

ScavTrap& ScavTrap::operator=(ScavTrap const & scavTrap)
{
    this->name = scavTrap.name;
    this->hit_points = scavTrap.hit_points;
    this->max_hit_points = scavTrap.max_hit_points;
    this->energy_points = scavTrap.energy_points;
    this->max_energy_points = scavTrap.max_energy_points;
    this->lvl = scavTrap.lvl;
    this->meel_attack = scavTrap.meel_attack;
    this->ranged_attack = scavTrap.ranged_attack;
    this->armor_damage = scavTrap.armor_damage;
    std::cout << "SC4G-TP "<< this->name << " assigned ???" << std::endl;
    return *this;
}

ScavTrap::~ScavTrap()
{
    std::cout << "SC4G-TP "<< this->name << " destroyed !" << std::endl;

}


void ScavTrap::challengeNewcomer(std::string const &target) {
    std::string list[] = {"Come here "+ target+ " !", "Fuck you " + target +"!", "Help !!!!!!!"};
    std::cout << list[rand()%3] << std::endl;
}