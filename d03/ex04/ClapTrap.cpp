//
// Created by AndreiMalcoci on 7/26/17.
//

#include "ClapTrap.hpp"
#include <iostream>
#include <iomanip>



ClapTrap::ClapTrap() {
    this->name = "ClapTrap";
    this->hit_points = 100;
    this->max_hit_points = 100;
    this->lvl = 1;
    std::cout << "TP "<< this->name << " appears !" << std::endl;
}

ClapTrap::ClapTrap(std::string name) {
    this->name = name;
    this->hit_points = 100;
    this->max_hit_points = 100;
    this->lvl = 1;
    std::cout <<"TP "<< this->name << " appears !" << std::endl;
}

ClapTrap::ClapTrap(ClapTrap const &clap)
{
    this->name = clap.name;
    this->hit_points = clap.hit_points;
    this->max_hit_points = clap.max_hit_points;
    this->energy_points = clap.energy_points;
    this->max_energy_points = clap.max_energy_points;
    this->lvl = clap.lvl;
    this->meel_attack = clap.meel_attack;
    this->ranged_attack = clap.ranged_attack;
    this->armor_damage = clap.armor_damage;
    std::cout << "TP "<< this->name << " cloned " << std::endl;
}

ClapTrap& ClapTrap::operator=(ClapTrap const &clap)
{
    this->name = clap.name;
    this->hit_points = clap.hit_points;
    this->max_hit_points = clap.max_hit_points;
    this->energy_points = clap.energy_points;
    this->max_energy_points = clap.max_energy_points;
    this->lvl = clap.lvl;
    this->meel_attack = clap.meel_attack;
    this->ranged_attack = clap.ranged_attack;
    this->armor_damage = clap.armor_damage;
    std::cout << "TP "<< this->name << " assigned ???" << std::endl;
    return *this;
}

ClapTrap::~ClapTrap() {
    std::cout << "TP "<< this->name << " destroyed !" << std::endl;
}

void ClapTrap::meleeAttack(std::string const &target) const {
    std::cout << "TP " << name << " attacks "<< target <<" at range, causing "<< meel_attack << " points of damage !" << std::endl;
}

void ClapTrap::rangedAttack(std::string const &target) const
{
    std::cout << "TP " << name << " attacks "<< target <<" at range, causing "<< ranged_attack << " points of damage !" << std::endl;
}

void ClapTrap::beRepaired(unsigned int amount)
{
    hit_points += amount;
    if (hit_points > 0)
        hit_points = max_hit_points;
    std::cout << "TP " << name << " healed "<< amount <<", hit become "<< hit_points << " !" << std::endl;
}

void ClapTrap::takeDamage(unsigned int amount) {
    hit_points -= amount - armor_damage;
    if (hit_points < 0)
        hit_points = 0;
    std::cout << "TP " << name << " damaged "<< amount - armor_damage <<", hit become "<< hit_points << " !" << std::endl;
}

std::ostream &operator<<(std::ostream &out, ClapTrap *clap)
{
    out << std::setw(5) << clap->name << "|"
              << std::setw(5)  << clap->hit_points << "|"
              << std::setw(5)  << clap->max_hit_points << "|"
              << std::setw(5)  << clap->energy_points << "|"
              << std::setw(5)  << clap->max_energy_points << "|"
              << std::setw(5)  << clap->lvl << "|"
              << std::setw(5)  << clap->meel_attack << "|"
              << std::setw(5)  << clap->ranged_attack << "|"
              << std::setw(5)  << clap->armor_damage;
    return out;
}