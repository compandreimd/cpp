#include <iostream>
#include "FragTrap.hpp"


FragTrap::FragTrap() : ClapTrap()
{
    this->name = "FragTrap";
    this->energy_points = 100;
    this->max_energy_points = 100;
    this->meel_attack = 30;
    this->ranged_attack = 20;
    this->armor_damage = 5;
    std::cout << "FR4G-TP "<< this->name << " appears !" << std::endl;
}

FragTrap::FragTrap(std::string name) : ClapTrap(name)
 {
    this->energy_points = 100;
    this->max_energy_points = 100;
    this->meel_attack = 30;
    this->ranged_attack = 20;
    this->armor_damage = 5;
    std::cout << "FR4G-TP "<< this->name << " appears !" << std::endl;
}

FragTrap::FragTrap(FragTrap const &frapTrap)
{
    this->name = frapTrap.name;
    this->hit_points = frapTrap.hit_points;
    this->max_hit_points = frapTrap.max_hit_points;
    this->energy_points = frapTrap.energy_points;
    this->max_energy_points = frapTrap.max_energy_points;
    this->lvl = frapTrap.lvl;
    this->meel_attack = frapTrap.meel_attack;
    this->ranged_attack = frapTrap.ranged_attack;
    this->armor_damage = frapTrap.armor_damage;
    std::cout << "FR4G-TP "<< this->name << " cloned " << std::endl;
}

FragTrap& FragTrap::operator=(FragTrap const &frapTrap)
{
    this->name = frapTrap.name;
    this->hit_points = frapTrap.hit_points;
    this->max_hit_points = frapTrap.max_hit_points;
    this->energy_points = frapTrap.energy_points;
    this->max_energy_points = frapTrap.max_energy_points;
    this->lvl = frapTrap.lvl;
    this->meel_attack = frapTrap.meel_attack;
    this->ranged_attack = frapTrap.ranged_attack;
    this->armor_damage = frapTrap.armor_damage;
    std::cout << "FR4G-TP "<< this->name << " assigned ???" << std::endl;
    return *this;
}

FragTrap::~FragTrap()
{
    std::cout << "FR4G-TP "<< this->name << " destroyed !" << std::endl;
}

void FragTrap::vaulthunter_dot_exe(std::string const &target) {
    if (energy_points >= 25) {
        energy_points -= 25;
        if (energy_points < 0)
            energy_points = 0;
        int attack = armor_damage + (rand() % meel_attack + rand() % ranged_attack);
        std::cout << "FR4G-TP " << name << " attacks " << target << " at vaulthunter, causing " << attack
                  << " points of damage !" << std::endl;
    } else
    {
        std::cout << "FR4G-TP " << name << " has low energy to attack !" << std::endl;
    }
}