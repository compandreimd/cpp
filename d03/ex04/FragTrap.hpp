//
// Created by AndreiMalcoci on 7/23/17.
//

#ifndef CPP_FRAPTRAP_HPP
#define CPP_FRAPTRAP_HPP

#include <string>
#include "ClapTrap.hpp"

class FragTrap : public virtual ClapTrap
{
public:
    FragTrap();
    FragTrap(std::string name);
    FragTrap(FragTrap const &frapTrap);
    FragTrap &operator=(FragTrap const &frapTrap);
    ~FragTrap();
    void vaulthunter_dot_exe(std::string const & target);
};

#endif