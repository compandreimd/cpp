#include <iostream>
#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"
#include "SuperTrap.hpp"

int main() {
    FragTrap *frap = new FragTrap("Frap");
    ScavTrap *scav = new ScavTrap("Scav");
    NinjaTrap *ninja = new NinjaTrap("Ninja");
    SuperTrap n("Super");
    std::cout << "---Table---" << std::endl;
    std::cout << "Name |HitPo|MaxHi|Enreg|MaxEn|Lvl  |Melee|Range|Armor" << std::endl;
    std::cout << scav << std::endl;
    std::cout << frap << std::endl;
    std::cout << ninja << std::endl;
    std::cout << &n << std::endl;
    n.ninajaTrap((ClapTrap*) frap);
    n.ninajaTrap(frap);
    n.ninajaTrap((ClapTrap*) scav);
    n.ninajaTrap(scav);
    n.ninajaTrap((ClapTrap*) ninja);
    n.ninajaTrap(ninja);
    delete frap;
    delete scav;
    delete ninja;
}