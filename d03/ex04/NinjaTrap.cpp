//
// Created by AndreiMalcoci on 7/26/17.
//

#include <iostream>
#include "NinjaTrap.hpp"


NinjaTrap::NinjaTrap() {
    this->name = "NinijaTrap";
    this->hit_points = 60;
    this->max_hit_points = 60;
    this->energy_points = 120;
    this->max_energy_points = 120;
    this->meel_attack = 60;
    this->ranged_attack = 5;
    this->armor_damage = 0;
}

NinjaTrap::NinjaTrap(std::string name) {
    this->name = name;
    this->hit_points = 60;
    this->max_hit_points = 60;
    this->energy_points = 120;
    this->max_energy_points = 120;
    this->meel_attack = 60;
    this->ranged_attack = 5;
    this->armor_damage = 0;
}

NinjaTrap::NinjaTrap(NinjaTrap const &trap) {
    this->name = trap.name;
    this->hit_points = trap.hit_points;
    this->max_hit_points = trap.max_hit_points;
    this->energy_points = trap.energy_points;
    this->max_energy_points = trap.max_energy_points;
    this->lvl = trap.lvl;
    this->meel_attack = trap.meel_attack;
    this->ranged_attack = trap.ranged_attack;
    this->armor_damage = trap.armor_damage;
}

NinjaTrap& NinjaTrap::operator=(NinjaTrap const &trap) {
    this->name = trap.name;
    this->hit_points = trap.hit_points;
    this->max_hit_points = trap.max_hit_points;
    this->energy_points = trap.energy_points;
    this->max_energy_points = trap.max_energy_points;
    this->lvl = trap.lvl;
    this->meel_attack = trap.meel_attack;
    this->ranged_attack = trap.ranged_attack;
    this->armor_damage = trap.armor_damage;
    return *this;
}

NinjaTrap::~NinjaTrap() {}

void NinjaTrap::ninajaTrap(ClapTrap *trap) {
    std::cout << name << ": How are you? "  << std::endl;
    std::cout << trap->name << ": I am " << trap->name << std::endl;
}

void NinjaTrap::ninajaTrap(FragTrap *trap) {
    std::cout << "Ninja Attack: " ;
    trap->takeDamage((unsigned)rand()%meel_attack);
}

void NinjaTrap::ninajaTrap(ScavTrap *trap) {
    std::cout << "Ninja Attack: " ;
    trap->takeDamage((unsigned)rand()%ranged_attack);
}

void NinjaTrap::ninajaTrap(NinjaTrap *trap) {
    std::cout << "Ninja Help: " ;
    trap->beRepaired((unsigned)rand()%meel_attack);
}