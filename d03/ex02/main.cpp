#include "FragTrap.hpp"
#include "ScavTrap.hpp"
int main() {
    ScavTrap a("A");
    ScavTrap b(a);
    FrapTrap *c = new FrapTrap("C");
    delete (c);
    a.takeDamage(10);
    a.beRepaired(120);
    a.takeDamage(123);
    c->meleeAttack("?");
    c->rangedAttack("?");
    a.meleeAttack("?");
    a.rangedAttack("?");
    a.challengeNewcomer("?");
    a.challengeNewcomer("?");
    a.challengeNewcomer("?");
    a.challengeNewcomer("?");
    a.challengeNewcomer("?");

}