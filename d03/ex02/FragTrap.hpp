//
// Created by AndreiMalcoci on 7/23/17.
//

#ifndef CPP_FRAPTRAP_HPP
#define CPP_FRAPTRAP_HPP

#include <string>
#include "ClapTrap.hpp"

class FrapTrap : public ClapTrap
{
public:
    FrapTrap();
    FrapTrap(std::string name);
    FrapTrap(FrapTrap const &frapTrap);
    FrapTrap &operator=(FrapTrap const &frapTrap);
    ~FrapTrap();
    void vaulthunter_dot_exe(std::string const & target);
};

#endif