//
// Created by AndreiMalcoci on 7/23/17.
//

#ifndef CPP_FRAPTRAP_HPP
#define CPP_FRAPTRAP_HPP

#include <string>

class FrapTrap
{
    int hit_points;
    int max_hit_points;
    int energy_points;
    int max_energy_points;
    int lvl;
    std::string name;
    int meel_attack;
    int ranged_attack;
    int armor_damage;
public:
    FrapTrap();
    FrapTrap(std::string name);
    FrapTrap(FrapTrap const &frapTrap);
    FrapTrap &operator=(FrapTrap const &frapTrap);
    ~FrapTrap();
    void rangedAttack(std::string const & target) const;
    void meleeAttack(std::string const & target) const;
    void takeDamage(unsigned int amount);
    void beRepaired(unsigned int amount);
    void vaulthunter_dot_exe(std::string const & target);
};

#endif