//
// Created by AndreiMalcoci on 7/26/17.
//

#include "scavTrap.hpp"

#include <iostream>
#include "FragTrap.hpp"


ScavTrap::ScavTrap() {
    this->name = "ScavTrap";
    this->hit_points = 100;
    this->max_hit_points = 100;
    this->energy_points = 50;
    this->max_energy_points = 50;
    this->lvl = 1;
    this->meel_attack = 20;
    this->ranged_attack = 15;
    this->armor_damage = 3;
    std::cout << "SC4G-TP "<< this->name << " appears !" << std::endl;
}

ScavTrap::ScavTrap(std::string name) {
    this->name = name;
    this->hit_points = 100;
    this->max_hit_points = 100;
    this->energy_points = 100;
    this->max_energy_points = 100;
    this->lvl = 1;
    this->meel_attack = 30;
    this->ranged_attack = 20;
    this->armor_damage = 5;
    std::cout << "SC4G-TP "<< this->name << " appears !" << std::endl;
}

ScavTrap::ScavTrap(ScavTrap const &scavTrap)
{
    this->name = scavTrap.name;
    this->hit_points = scavTrap.hit_points;
    this->max_hit_points = scavTrap.max_hit_points;
    this->energy_points = scavTrap.energy_points;
    this->max_energy_points = scavTrap.max_energy_points;
    this->lvl = scavTrap.lvl;
    this->meel_attack = scavTrap.meel_attack;
    this->ranged_attack = scavTrap.ranged_attack;
    this->armor_damage = scavTrap.armor_damage;
    std::cout << "SC4G-TP "<< this->name << " cloned " << std::endl;
}

ScavTrap& ScavTrap::operator=(ScavTrap const & scavTrap)
{
    this->name = scavTrap.name;
    this->hit_points = scavTrap.hit_points;
    this->max_hit_points = scavTrap.max_hit_points;
    this->energy_points = scavTrap.energy_points;
    this->max_energy_points = scavTrap.max_energy_points;
    this->lvl = scavTrap.lvl;
    this->meel_attack = scavTrap.meel_attack;
    this->ranged_attack = scavTrap.ranged_attack;
    this->armor_damage = scavTrap.armor_damage;
    std::cout << "SC4G-TP "<< this->name << " assigned ???" << std::endl;
    return *this;
}

ScavTrap::~ScavTrap() {
    std::cout << "SC4G-TP "<< this->name << " destroyed !" << std::endl;
}

void ScavTrap::meleeAttack(std::string const &target) const {
    std::cout << "SC4G-TP " << name << " attacks "<< target <<" at range, causing "<< meel_attack << " points of damage !" << std::endl;
}

void ScavTrap::rangedAttack(std::string const &target) const
{
    std::cout << "SC4G-TP " << name << " attacks "<< target <<" at range, causing "<< ranged_attack << " points of damage !" << std::endl;
}

void ScavTrap::beRepaired(unsigned int amount)
{
    hit_points += amount;
    if (hit_points > 0)
        hit_points = max_hit_points;
    std::cout << "SC4G-TP " << name << " healed "<< amount <<", hit become "<< hit_points << " !" << std::endl;
}

void ScavTrap::takeDamage(unsigned int amount) {
    hit_points -= amount - armor_damage;
    if (hit_points < 0)
        hit_points = 0;
    std::cout << "SC4G-TP " << name << " damaged "<< amount - armor_damage << ", hit become "<< hit_points << " !" << std::endl;
}

void ScavTrap::challengeNewcomer(std::string const &target) {
    std::string list[] = {"Come here "+ target+ " !", "Fuck you " + target +"!", "Help !!!!!!!"};
    std::cout << list[rand()%3] << std::endl;
}