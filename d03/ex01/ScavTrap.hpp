//
// Created by AndreiMalcoci on 7/26/17.
//

#ifndef CPP_SCAVTRAP_HPP
#define CPP_SCAVTRAP_HPP


#include <string>

class ScavTrap
{
private:
    int hit_points;
    int max_hit_points;
    int energy_points;
    int max_energy_points;
    int lvl;
    std::string name;
    int meel_attack;
    int ranged_attack;
    int armor_damage;
public:
    ScavTrap();
    ScavTrap(std::string name);
    ScavTrap(ScavTrap const &scavTrap);
    ScavTrap &operator=(ScavTrap const &sravTrap);
    ~ScavTrap();
    void rangedAttack(std::string const & target) const;
    void meleeAttack(std::string const & target) const;
    void takeDamage(unsigned int amount);
    void beRepaired(unsigned int amount);
    void challengeNewcomer(std::string const &traget);
};


#endif //CPP_SCAVTRAP_HPP
