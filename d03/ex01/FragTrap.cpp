#include <iostream>
#include "FragTrap.hpp"


FrapTrap::FrapTrap() {
    this->name = "FrapTrap";
    this->hit_points = 100;
    this->max_hit_points = 100;
    this->energy_points = 100;
    this->max_energy_points = 100;
    this->lvl = 1;
    this->meel_attack = 30;
    this->ranged_attack = 20;
    this->armor_damage = 5;
    std::cout << "FR4G-TP "<< this->name << " appears !" << std::endl;
}

FrapTrap::FrapTrap(std::string name) {
    this->name = name;
    this->hit_points = 100;
    this->max_hit_points = 100;
    this->energy_points = 100;
    this->max_energy_points = 100;
    this->lvl = 1;
    this->meel_attack = 30;
    this->ranged_attack = 20;
    this->armor_damage = 5;
    std::cout << "FR4G-TP "<< this->name << " appears !" << std::endl;
}

FrapTrap::FrapTrap(FrapTrap const &frapTrap)
{
    this->name = frapTrap.name;
    this->hit_points = frapTrap.hit_points;
    this->max_hit_points = frapTrap.max_hit_points;
    this->energy_points = frapTrap.energy_points;
    this->max_energy_points = frapTrap.max_energy_points;
    this->lvl = frapTrap.lvl;
    this->meel_attack = frapTrap.meel_attack;
    this->ranged_attack = frapTrap.ranged_attack;
    this->armor_damage = frapTrap.armor_damage;
    std::cout << "FR4G-TP "<< this->name << " cloned " << std::endl;
}

FrapTrap& FrapTrap::operator=(FrapTrap const &frapTrap)
{
    this->name = frapTrap.name;
    this->hit_points = frapTrap.hit_points;
    this->max_hit_points = frapTrap.max_hit_points;
    this->energy_points = frapTrap.energy_points;
    this->max_energy_points = frapTrap.max_energy_points;
    this->lvl = frapTrap.lvl;
    this->meel_attack = frapTrap.meel_attack;
    this->ranged_attack = frapTrap.ranged_attack;
    this->armor_damage = frapTrap.armor_damage;
    std::cout << "FR4G-TP "<< this->name << " assigned ???" << std::endl;
    return *this;
}

FrapTrap::~FrapTrap() {
    std::cout << "FR4G-TP "<< this->name << " destroyed !" << std::endl;
}

void FrapTrap::meleeAttack(std::string const &target) const {
    std::cout << "FR4G-TP " << name << " attacks "<< target <<" at range, causing "<< meel_attack << " points of damage !" << std::endl;
}

void FrapTrap::rangedAttack(std::string const &target) const
{
    std::cout << "FR4G-TP " << name << " attacks "<< target <<" at range, causing "<< ranged_attack << " points of damage !" << std::endl;
}

void FrapTrap::beRepaired(unsigned int amount)
{
    hit_points += amount;
    if (hit_points > 0)
        hit_points = max_hit_points;
    std::cout << "FR4G-TP " << name << " healed "<< amount <<", hit become "<< hit_points << " !" << std::endl;
}

void FrapTrap::takeDamage(unsigned int amount) {
    hit_points -= amount - armor_damage;
    if (hit_points < 0)
        hit_points = 0;
    std::cout << "FR4G-TP " << name << " damaged "<< amount - armor_damage <<", hit become "<< hit_points << " !" << std::endl;
}

void FrapTrap::vaulthunter_dot_exe(std::string const &target) {
    if (energy_points >= 25) {
        energy_points -= 25;
        if (energy_points < 0)
            energy_points = 0;
        int attack = armor_damage + (rand() % meel_attack + rand() % ranged_attack);
        std::cout << "FR4G-TP " << name << " attacks " << target << " at vaulthunter, causing " << attack
                  << " points of damage !" << std::endl;
    } else
    {
        std::cout << "FR4G-TP " << name << " has low energy to attack !" << std::endl;
    }
}