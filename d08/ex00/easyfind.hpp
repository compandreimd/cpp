/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   easyfind.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/01 09:16:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/08/01 09:16:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef CPP_EASYFIND_HPP
#define CPP_EASYFIND_HPP

#include <algorithm>
#include <stdexcept>

template <typename T>
int easyfind(T &t, int n) throw()
{
	typename T::iterator it = std::find(t.begin(), t.end(),  n);
	if(it != t.end())
		return *it;
	throw std::logic_error("Not Founded :(");
}

#endif //CPP_EASYFIND_HPP
