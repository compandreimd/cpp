#include <stack>

#ifndef MUTANTSTACK_HPP
#define MUTANTSTACK_HPP

template <typename T>
class MutantStack : public std::stack<T>
{
	typedef std::stack <T> base;
public:
	MutantStack(){}
	~MutantStack(){}
	MutantStack(MutantStack const &stack): base(stack){}
	typedef typename base::container_type::iterator iterator;

	iterator begin(){return base::c.begin();}
	iterator end(){return base::c.end();}
};
#endif
