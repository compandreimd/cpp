/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/01 10:04:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/08/01 11:14:24 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "mutantstack.hpp"

int main()
{
	MutantStack<int> mstack;

	mstack.push(5);
	mstack.push(17);
	std::cout << mstack.top() << std::endl;
	mstack.pop();
	std::cout << mstack.size() << std::endl;
   	mstack.push(11);
	//...
	MutantStack<int>::iterator itb = mstack.begin();
   	MutantStack<int>::iterator ite = mstack.end();
	++itb;
	--ite;
	while(itb != ite)
	{
		std::cout << *itb << std::endl;
		++itb;
	}	
	std::stack<int> s(mstack);
	return 0;
}
