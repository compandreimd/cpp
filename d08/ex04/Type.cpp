//
// Created by andreimalcoci on 8/3/17.
//

#include "Type.h"
#include <iostream>

Type::Type() {
    op = 0;
}

Type::Type(Type const &t) {
    this->i = t.i;
    this->op = t.op;
}

Type& Type::operator=(Type const &t) {
    this->i = t.i;
    this->op = t.op;
    return *this;
}

Type::~Type() {

}

int Type::getI() const
{
    return i;
}

void Type::setI(int i)
{
    op = 2;
    this->i = i;
}

void Type::setC(char c)
{
    if (c == '+' || c == '-' || c == '*' || c == '/') {
        setI(c);
        op = 1;
    }
    else if(c == '(')
        op = 3;
    else if (c == ')')
        op = 4;
    else
        op = 0;
}

char Type::getC() const
{
    return (char)i;
}

int Type::getTypeID() const
{
    return op;
};

std::string const Type::getType() const
{
    std::string list[] = {"HZ", "Op", "Num", "ParOpen", "ParClose" };
    return list[op];
};

std::ostream &operator<<(std::ostream &out,Type const &t)
{
    out << t.getType();
    if(t.getTypeID() == 0)
    {
        out << "(" << t.getC() << ")";
        out << "(" << t.getI() << ")";
    }
    else if (t.getTypeID() == 1)
        out << "(" << t.getC() << ")";
    else if(t.getTypeID() == 2)
        out << "(" << t.getI() << ")";
    return out;
}


std::istream &operator>>(std::istream &in, Type &op)
{
    char c = '?';
    in >> c;
    if (isdigit(c) > 0)
    {
        int i = 0;
        while(isdigit(c) > 0 && in) {
            i *= 10;
            i += (c - '0');
            in >> c;
        }
        in.putback(c);
        op.setI(i);
    }
    else{
        op.setC(c);
    }
    return in;
}