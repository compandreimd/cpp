//
// Created by andreimalcoci on 8/3/17.
//

#ifndef CPP_TYPE_H
#define CPP_TYPE_H

#include <string>

class Type {

    int i;
    int op;
public:
    Type();

    Type(Type const &t);

    Type &operator=(Type const &t);

    ~Type();

    int getI() const;

    void setI(int i);

    void setC(char c);

    char getC() const;

    int getTypeID() const;

    std::string const getType() const;

};

std::ostream &operator<<(std::ostream &out, Type const &t);

std::istream &operator>>(std::istream &in, Type &op);

#endif //CPP_TYPE_H
