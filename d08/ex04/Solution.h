//
// Created by andreimalcoci on 8/3/17.
//

#ifndef CPP_SOLUTION_H
#define CPP_SOLUTION_H


#include <vector>
#include "Type.h"

class Solution {

    void infixToPostfix(const std::vector<Type> &infix, std::vector<Type> &postfix) const;

    int precedence(Type const &x) const;

    std::vector<Type> expression;

    Solution();
public:
    /**
     * @param expression: A string array
     * @return: The Reverse Polish notation of this expression
     */
    std::vector<Type> getRPN() const;

    Solution(char const *str);

    Solution(std::vector<Type> expression);

    Solution(Solution const &s);

    Solution &operator=(Solution const &s);

    ~Solution();

    // Convert Infix to Postfix Expression.
    const std::vector<Type> &getExpression() const;

    int toCalc(int debug) const;
};

std::ostream &operator<<(std::ostream &out, Solution const &s);
std::ostream &operator<<(std::ostream &out, std::stack<int> const &exp);
std::ostream &operator<<(std::ostream &out, std::vector<Type> const &exp);


#endif //CPP_SOLUTION_H
