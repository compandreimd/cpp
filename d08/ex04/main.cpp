

#include <string>
#include <vector>
#include <stack>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include "Solution.h"

int main(int ac, char *av[])
{
    if(ac == 2) {
		try
		{
			Solution solution(av[1]);
			std::cout << solution;
		}
		catch (std::exception &error)
		{
			std::cout << error.what();
		}
    }
    else
    {
        std::cout << "using: app expreion";
    }
    return 0;
}
