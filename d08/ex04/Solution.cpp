//
// Created by andreimalcoci on 8/3/17.
//

#include <stack>
#include "Solution.h"
#include <iostream>
#include <sstream>
#include <iomanip>


Solution::Solution() {
}

Solution::Solution(char const *str)
{
    std::stringstream sstr(str);
    while(sstr) {
        Type type;
        sstr >> type;
        expression.push_back(type);
    }
}

Solution::Solution(std::vector<Type> expression) {
    this->expression = expression;
}

Solution::Solution(Solution const &s) {
    this->expression = s.expression;
}

Solution& Solution::operator=(Solution const &s) {
    this->expression = s.expression;
    return *this;
}

Solution::~Solution() {}

std::vector<Type> Solution::getRPN() const {
    std::vector<Type> output;
    infixToPostfix(expression, output);
    return output;
}


void Solution::infixToPostfix(const std::vector<Type> &infix, std::vector<Type> &postfix) const {
    std::stack<Type> s;
    std::vector<Type>::const_iterator tok, tend;
    for (tend = infix.end(), tok = infix.begin(); tok != tend; ++tok) {
        Type stok = *tok;
        if (stok.getTypeID() == 2) {
            postfix.push_back(stok);
        } else if (stok.getTypeID() == 3) {
            s.push(stok);
        } else if (stok.getTypeID() == 4) {
            while (!s.empty()) {
                stok = s.top();
                s.pop();
                if (stok.getTypeID() == 3) {
                    break;
                }
                postfix.push_back(stok);
            }
        } else {
            while (!s.empty() && precedence(stok) <= precedence(s.top())) {
                postfix.push_back(s.top());
                s.pop();
            }
            s.push(stok);
        }
    }
    while (!s.empty()) {
        postfix.push_back(s.top());
        s.pop();
    }
}

int Solution::precedence(Type const &x) const {
    if (x.getTypeID() == 3) {
        return 0;
    } else if (x.getC() == '+' || x.getC() == '-') {
        return 1;
    } else if (x.getC() == '*' || x.getC() == '/') {
        return 2;
    }
    return 3;
}



int Solution::toCalc(int debug) const {
    std::vector<Type> exp = getRPN();
    std::stack<int> stack;
    for(size_t i = 0; i < exp.size(); i++)
    {
        if(exp[i].getTypeID() == 2)
        {
            stack.push(exp[i].getI());
            if (debug > 0)
            {
                std::cout << "I " << exp[i] << "\t|\tOP Push\t\t\t|\t ST " << stack << "]"<< std::endl;
            }
        }
        else if(exp[i].getTypeID() == 1)
        {
            if (exp[i].getC() == '+')
            {
                int a = stack.top();
                stack.pop();
                int b = stack.top();
                stack.pop();
                stack.push(a + b);
                if (debug > 0) {
                    std::cout << "I " << exp[i] << "\t\t|\tOP Add\t\t\t|\t ST " << stack << "]" << std::endl;
                }
            }
            else if (exp[i].getC() == '*')
            {
                int a = stack.top();
                stack.pop();
                int b = stack.top();
                stack.pop();
                stack.push(a * b);
                if (debug > 0) {
                    std::cout << "I " << exp[i] << "\t\t|\tOP Multiply\t\t|\t ST " << stack << "]" << std::endl;
                }
            }
            else if (exp[i].getC() == '-')
            {
                int a = stack.top();
                stack.pop();
                int b = stack.top();
                stack.pop();
                stack.push(b - a);
                if (debug > 0) {
                    std::cout << "I " << exp[i] << "\t\t|\tOP Substract\t|\t ST " << stack << "]" << std::endl;
                }
            }
            else if (exp[i].getC() == '/')
            {
                int a = stack.top();
                stack.pop();
                int b = stack.top();
                stack.pop();
                stack.push(b / a);
                if (debug > 0) {
                    std::cout << "I " << exp[i] << "\t\t|\tOP Div\t\t\t|\t ST " << stack << "]" << std::endl;
                }
            }
        }
    }
    return stack.top();
}

const std::vector<Type> &Solution::getExpression() const {
    return expression;
}

std::ostream &operator<<(std::ostream &out, std::vector<Type> const &exp)
{
    for(size_t i = 0; i < exp.size() - 1; i++)
        if(exp[i].getTypeID() > 0)
        out << exp[i] << " ";
    if(exp[exp.size()].getTypeID() > 0)
        out << exp[exp.size() - 1];
    return out;
}

std::ostream &operator<<(std::ostream &out, std::stack<int> const &exp)
{
    std::stack<int> s = exp;
    while(s.size() - 1 > 0)
    {
        out << s.top() << " ";
        s.pop();
    }
    out << s.top();
    return out;
}

std::ostream &operator<<(std::ostream &out, Solution const &s)
{
    out << "Tokens: " << s.getExpression() << std::endl
        << "Postfix: " << s.getRPN() << std::endl;
    int rs = s.toCalc(1);
    out << "Result: " << rs << std::endl;
    return out;
}

