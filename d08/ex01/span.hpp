/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/01 09:39:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/08/01 09:39:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef CPP_SPAN_HPP
#define CPP_SPAN_HPP


#include <vector>

class Span {
	std::vector<int> v;
	int max;
public:
	Span(unsigned N=1);
	void addNumber(int i) throw();
	unsigned shortestSpan() throw();
	unsigned longestSpan() throw();
};


#endif //CPP_SPAN_HPP
