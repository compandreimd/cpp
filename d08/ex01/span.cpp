/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/01 09:39:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/08/01 09:39:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "span.hpp"
#include <stdexcept>

Span::Span(unsigned int N) {
	this->max = N;
	v.reserve(N);
}

void Span::addNumber(int i) throw(){
	if(v.size() < v.capacity())
		v.push_back(i);
	else throw std::logic_error("Capacity is full!");
}

unsigned Span::shortestSpan() throw(){
	if (v.size() <= 1)
		throw std::logic_error("Error we have only one");
	unsigned  min  = UINT_MAX;
	for(unsigned long i = 0; i < v.size() - 1; i++)
	{
		unsigned diff = v[i] > v[i+1] ? v[i] - v[i+1] : v[i+1] - v[i];
		if(min > diff)
			min = diff;
	}
	return min;
}

unsigned Span::longestSpan() throw(){
	if (v.size() <= 1)
		throw std::logic_error("Error we have only one");
	unsigned  max  = 0;
	for(unsigned long i = 0; i < v.size() - 1; i++)
	{
		unsigned diff = v[i] > v[i+1] ? v[i] - v[i+1] : v[i+1] - v[i];
		if(max < diff)
			max = diff;
	}
	return max;
}