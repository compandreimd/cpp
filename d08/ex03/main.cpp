


#include <string>
#include <fstream>
#include <iostream>
#include "MaindFuck.hpp"

int main(int ac, char *av[])
{
	if(ac == 2)
	{
		std::vector<std::string> program;
		std::ifstream file(av[1]);
		while(!file.eof())
		{
			std::string str;
			file >> str;
			if (MaindFuck::isOk(str))
				program.push_back(str);
		}
		try {
			MaindFuck *f = new MaindFuck(program);
			f->evaluate();
		}
		catch (std::exception ex)
		{
			std::cout << "error";
		}
	}
	else
		std::cout << "using app file" << std::endl;
}
