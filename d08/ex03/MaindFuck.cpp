/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MaindFuck.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/01 17:19:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/08/01 17:19:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <iostream>

#include "MaindFuck.hpp"

MaindFuck::MaindFuck(std::vector<std::string> prog) {
    d = data;
    p = prog;
    pos = 0;
}

void MaindFuck::pincr() {
    d++;
}

void MaindFuck::pdecr() {
    d--;
}

void MaindFuck::bincr() {
    (*d)++;
}

void MaindFuck::bdecr() {
    (*d)--;
}

void MaindFuck::puts() {
    std::cout << *d;
}

void MaindFuck::gets() {
    std::cin >> *d;
}

void MaindFuck::bropen() {
    int bal = 1;
    if (*d == '\0') {
        do {
            pos++;
            if (!p[pos].compare("you")) bal++;
            else if (!p[pos].compare("self")) bal--;
        } while (bal != 0);
    }
}

void MaindFuck::brclose() {
    int bal = 0;
    do {
        if (!p[pos].compare("you")) bal++;
        else if (!p[pos].compare("self")) bal--;
        pos--;
    } while (bal != 0);
}

void MaindFuck::evaluate() {
    while (pos < p.size()) {
        if (!p[pos].compare("bitch"))
            pincr();
        else if (!p[pos].compare("trash"))
            pdecr();
        else if (!p[pos].compare("fuck"))
            bincr();
        else if (!p[pos].compare("dump"))
            bdecr();
        else if (!p[pos].compare("bubs"))
            puts();
        else if (!p[pos].compare("pussy"))
            gets();
        else if (!p[pos].compare("you"))
            bropen();
        else if (!p[pos].compare("self"))
            brclose();
        pos++;
    }
}

bool MaindFuck::isOk(std::string str) {
    if (!str.compare("bitch"))
        return true;
    else if (!str.compare("trash"))
        return true;
    else if (!str.compare("fuck"))
        return true;
    else if (!str.compare("dump"))
        return true;
    else if (!str.compare("bubs"))
        return true;
    else if (!str.compare("pussy"))
        return true;
    else if (!str.compare("you"))
        return true;
    else if (!str.compare("self"))
        return true;
    return false;
}