/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MaindFuck.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/01 17:19:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/08/01 17:19:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef CPP_BRAINFUCK_HPP
#define CPP_BRAINFUCK_HPP

#include <string.h>
#include <vector>


class MaindFuck {
	char data[30000];
	char  *d;
	std::vector<std::string> p;
	size_t pos;

	void pincr();

	void pdecr();

	void bincr();

	void bdecr();

	void puts();

	void gets();

	void bropen();

	void brclose();

public:


	MaindFuck(std::vector<std::string> prog);

	static bool isOk(std::string str);

	void evaluate();
};


#endif //CPP_BRAINFUCK_HPP
