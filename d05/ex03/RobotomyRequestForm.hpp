#ifndef ROBOTOMYREQUESTFORM_HPP

/*
** defines
*/
# define ROBOTOMYREQUESTFORM_HPP

/*
** includes
*/
# include "Form.hpp"

/*
** class
*/
class RobotomyRequestForm : public Form
{
	public:
		RobotomyRequestForm(std::string const &target = "TargetLess");
		RobotomyRequestForm(RobotomyRequestForm const &copy);
		virtual ~RobotomyRequestForm(void);
		RobotomyRequestForm		&operator=(RobotomyRequestForm const &copy);

		virtual void				execute(Bureaucrat const &executor) const;

	private:
		std::string					_target;
};

#endif //PRESIDENTIALPARDONFORM_HPP
