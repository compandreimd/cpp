#include "Bureaucrat.hpp"

int		main(void)
{
	Bureaucrat a("a", 5);
	Bureaucrat b("b", 147);

	try
	{
		Bureaucrat *c = new Bureaucrat("c", 1000);
		delete c;
	}
	catch (std::exception &e)
	{
		std::cout << "Error: " << e.what() << std::endl;
	}

	for (int i = 0; i < 8; i++)
	{
		try
		{
			a += 1;
			b -= 1;
		}
		catch (std::exception &e)
		{
			std::cout << "Error: " << e.what() << std::endl;
		}
		std::cout << a;
		std::cout << b;
	}

	return (0);
}
