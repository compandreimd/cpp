//
// Created by AndreiMalcoci on 7/30/17.
//

#ifndef CPP_OFFICEBLOCK_HPP
#define CPP_OFFICEBLOCK_HPP


#include "Intern.hpp"

class OfficeBlock {
    Intern *intern;
    Bureaucrat *signer;
    Bureaucrat *executor;

public:
    OfficeBlock();
    void setIntern(Intern &intern);
    void setSigner(Bureaucrat &singner);
    void setExecutor(Bureaucrat &excutor);
    void doBureaucracy(std::string name, std::string target) throw();
};


#endif //CPP_OFFICEBLOCK_HPP
