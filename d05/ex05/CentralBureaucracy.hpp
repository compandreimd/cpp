//
// Created by AndreiMalcoci on 7/30/17.
//

#ifndef CPP_CENTRALBUREAUCRACY_HPP
#define CPP_CENTRALBUREAUCRACY_HPP


#include "OfficeBlock.hpp"

class CentralBureaucracy {
    OfficeBlock offices[20];
    Bureaucrat bureaucrats[20];
public:
    CentralBureaucracy();
    void doBureaucracy(int k);

};


#endif //CPP_CENTRALBUREAUCRACY_HPP
