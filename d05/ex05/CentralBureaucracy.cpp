//
// Created by AndreiMalcoci on 7/30/17.
//

#include "CentralBureaucracy.hpp"

CentralBureaucracy::CentralBureaucracy() {
    for(int i = 0; i < 19; i++)
    {
        offices[i].setExecutor(bureaucrats[i]);
        offices[i].setSigner(bureaucrats[i + 1]);
        int n = 10 + i * 5;
        bureaucrats[i].setGrade(n);


    }
    offices[19].setExecutor(bureaucrats[19]);
    offices[19].setSigner(bureaucrats[0]);
}


void CentralBureaucracy::doBureaucracy(int k) {
    for(int i = 0 ; i < k; i++)
    {
        Intern intern;
        offices[i % 20].setIntern(intern);
        try {
            offices->doBureaucracy(std::to_string(i % 4), std::to_string(i));
        }
        catch (std::exception &e)
        {
            std::cout << e.what();
        }
    }
}