//
// Created by AndreiMalcoci on 7/30/17.
//

#include "OfficeBlock.hpp"

OfficeBlock::OfficeBlock() {

}
void OfficeBlock::setIntern(Intern &intern) {
    this->intern = &intern;
}

void OfficeBlock::setSigner(Bureaucrat &singner) {
    this->signer = &singner;
}

void OfficeBlock::setExecutor(Bureaucrat &excutor) {
    this->executor = &excutor;
}

void OfficeBlock::doBureaucracy(std::string name, std::string target) throw() {
    Form *f = intern->makeForm(name, target);
    if (f) {
        signer->signForm(f);
        executor->executeForm(f);

        if (f->getIsSigned()) {
            std::cout << "That'll do, " << f->getTarget() << ". That'll do ..." << std::endl;
        } else {
            std::cout << "Rejected!!!" << std::endl;
        }
    }
    //std::cout << "Intern creates a " f
}