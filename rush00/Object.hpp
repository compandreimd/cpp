//
// Created by AndreiMalcoci on 7/22/17.
//

#ifndef CPP_OBJECT_HPP
#define CPP_OBJECT_HPP

#include <iostream>
#include <ncurses.h>
#include "Vector.hpp"

class Unit;

class Object {
protected:
    int x;
    int y;
    wchar_t c;
    int live;
    int armour;
    int type;
    static Vector<Object *> *list;

public:
    Object();

    Object(wchar_t c, int x, int y);

    Object(Object const &obj);

    Object &operator=(Object const &obj);

    virtual ~Object();

    Object *setX(int x);

    Object *setY(int y);

    Object *setLive(int l);

    Object *setArmour(int a);

    Object *setC(wchar_t c);

    int getX() const;

    int getY() const;

    int getType() const;

    wchar_t getChar() const;

    virtual void draw(WINDOW *window);

    static Vector<Object *> *getList();

    virtual bool onColusion(Object *object);

    virtual bool collusion();

    int getLive() const;

    int getArmour() const;
};

std::ostream &operator<<(std::ostream &out, Object const &ob);


#endif //CPP_OBJECT_HPP
