//
// Created by AndreiMalcoci on 7/20/17.
//

#ifndef CPP_LOGGER_H
#define CPP_LOGGER_H

#define LogConsole "Console"
#define LogFile "File"
#define Log "All"

#include <string>

class Logger
{
private:
    std::string file_name;

    std::string option[3];

    void (Logger::*list_f[2])(std::string const &);

    void logToConsole(std::string const &log);

    void logToFile(std::string const &log);

    std::string makeLogEntry(std::string const &log) const;

public:
    Logger(std::string name);

    void log(std::string const &dest, std::string const &message);

    static Logger *main;

};


#endif //CPP_LOGGER_H
