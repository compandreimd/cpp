//
// Created by AndreiMalcoci on 7/23/17.
//

#ifndef CPP_UNIT_HPP
#define CPP_UNIT_HPP


#include "Object.hpp"
#include "Bullet.hpp"

class Unit : public Object {
private:
    int drawed;
public:
    int points;

    int power;

    static index_int units;

    Unit();

    Unit(wchar_t c);

    Unit(Unit const &u);

    Unit &operator=(Unit const &u);

    ~Unit();

    void move();

    Bullet *fire();

    static Unit *randomUnit(WINDOW *w);

    void draw(WINDOW *w);

    bool onColusion(Object *object);
};


#endif //CPP_UNIT_HPP
