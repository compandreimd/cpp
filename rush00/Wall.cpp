//
// Created by root on 7/23/17.
//

#include <cstdlib>
#include "Wall.hpp"

Vector <Object *> *Wall::getUp() const
{
	return up;
}

Vector <Object *> *Wall::getDown() const
{
	return down;
}

void Wall::setPos(WINDOW *w, int pos)
{
	int my = getmaxy(w);
	for(index_int i = 0; i < up->length(); i++)
        up->operator[](i)->setX(pos)->setY(i);
    for(index_int i = 0; i < down->length(); i++)
        down->operator[](i)->setX(pos)->setY(my-i-1);

}


void Wall::draw(WINDOW *window) {
    for(index_int i = 0; i < up->length(); i++)
		up->operator[](i)->draw(window);

    for(index_int i = 0; i < down->length(); i++)
        down->operator[](i)->draw(window);
}

Wall::Wall()
{
	up = new Vector<Object*>();
	down = new Vector<Object*>();
	up->push(new Object());
	down->push(new Object());
}

Wall::Wall(int height)
{
	int u = rand() % height +1;
	int d = rand() % height +1;
	up = new Vector<Object*>();
	down = new Vector<Object*>();
	for(int i = 0; i < u; i++)
	{
		up->push(new Object());
	}
	for(int i = 0; i < d; i++)
	{
		down->push(new Object());
	}
}

Wall::Wall(Wall const &w)
{
	*up = *w.up;
	*down = *w.down;
}

Wall& Wall::operator=(Wall const &w)
{
	*up = *w.up;
	*down = *w.down;
	return *this;
}

Wall::~Wall()
{
	int l = up->length();
	for(int i=0;i<l;i++)
	{
		Object* object = up->pop();
		delete object;
	}
	l =down->length();
	for(int i=0;i<l;i++)
	{
		Object* object = down->pop();
		delete object;
	}
	delete up;
	delete down;
}