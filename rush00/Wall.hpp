//
// Created by root on 7/23/17.
//

#ifndef CPP_WALL_HPP
#define CPP_WALL_HPP


#include "Object.hpp"

class Wall
{
	Vector<Object*> *up;
	Vector<Object*> *down;
public:
	Wall();
	Wall(int height);
	Wall(Wall const &w);
	Wall &operator=(Wall const &w);
	~Wall();
	void setPos(WINDOW *window, int pos);
	void draw(WINDOW *window);
	Vector <Object *> *getUp() const;
	Vector <Object *> *getDown() const;
};


#endif //CPP_WALL_HPP
