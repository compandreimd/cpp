//
// Created by AndreiMalcoci on 7/20/17.
//

#include <iostream>
#include <fstream>
#include "Logger.hpp"

Logger *Logger::main = new Logger("app.log");

Logger::Logger(std::string name) {
    file_name = name;
    option[0] = "Console";
    list_f[0] = &Logger::logToConsole;
    option[1] = "File";
    list_f[1] = &Logger::logToFile;
    option[2] = "All";
}

void Logger::log(std::string const &dest, std::string const &message) {
    for (int i = 0; i < 2; i++)
        if (!dest.compare(option[i])) {
            (this->*list_f[i])(message);
        }
    if (!dest.compare(option[2])) {
        for (int i = 0; i < 2; i++)
            (this->*list_f[i])(message);
    }
}

void Logger::logToConsole(std::string const &log) {
    std::cout << makeLogEntry(log) << std::endl;
}

void Logger::logToFile(std::string const &log) {
    std::fstream fs;
    fs.exceptions(std::fstream::failbit | std::fstream::badbit);
    try {
        fs.open(file_name, std::fstream::in | std::fstream::out | std::fstream::app);
        fs << makeLogEntry(log) << std::endl;
        fs.close();
    }
    catch (std::fstream::failure) {
        std::cerr << "Somthing go wrong :(" << std::endl;
    }
}



std::string Logger::makeLogEntry(std::string const &log) const {
    time_t theTime = time(NULL);
    struct tm *aTime = localtime(&theTime);
    char s[32];

    strftime(s, sizeof(s), "[%d.%m.%Y %H:%M:%S] ", aTime);
    return s + log;
}