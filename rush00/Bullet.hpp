//
// Created by AndreiMalcoci on 7/23/17.
//

#ifndef CPP_BULLET_HPP
#define CPP_BULLET_HPP


#include "Object.hpp"

class Bullet : public Object
{
    int direction;
    Unit *parent;
public:
    Bullet(int x,int y, Unit *parent);
    bool onColusion(Object *object);
    void draw(WINDOW *w);
    Bullet *setDirection(int i);
    Bullet *setChar(char c);
};


#endif //CPP_BULLET_HPP
