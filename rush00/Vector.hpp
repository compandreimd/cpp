//
// Created by AndreiMalcoci on 7/22/17.
//

#ifndef CPP_VECTOR_HPP
#define CPP_VECTOR_HPP

#include <iostream>
typedef unsigned index_int;

template <typename T>
class Vector;

template <typename T>
class Vector
{
	T *list;
	index_int len;
public:
	Vector()
	{
		len = 0;
		list = new T[len];
	}

	Vector(index_int size)
	{
		len = size;
		list = new T[len];
	}

	Vector(Vector <T> const &v)
	{
		len = v.len;
		list = new T[len];
		for (index_int i = 0; i < len; i++)
		{
			list[i] = T(v.list[i]);
		}
	}

	Vector <T> &operator=(Vector <T> const &v)
	{
		len = v.len;
		list = new T[len];
		for (index_int i = 0; i < len; i++)
		{
			list[i] = T(v.list[i]);
		}
		return *this;
	}

	~Vector()
	{
		delete[] list;
	}

	T &operator[](index_int i)
	{
		return list[i];
	}

	T operator[](index_int i) const
	{
		return list[i];
	}

	T pop(index_int k)
	{
		if (len > 0)
		{
			len--;
			T el = list[k];
			T *n = new T[len];
			for (index_int i = 0; i < len; i++)
			{
				index_int p = i;
				if (p >= k)
					p++;
				n[i] = list[p];
			}

			delete[]list;
			list = n;
			return el;
		}
		return T();
	}

	T pop()
	{
		if (len > 0)
		{
			len--;
			T el = list[len];
			T *n = new T[len];
			for (index_int i = 0; i < len; i++)
				n[i] = list[i];
			delete[]list;
			list = n;
			return el;
		}
		return T();
	}

	index_int pop(T t)
	{
		for(index_int i=0;i < len; i++)
		{
			if ((*this)[i] == t)
			{
				pop(i);
				return i;
			}
		}
		return -1;
	}

	index_int push(T el)
	{
		T *n = new T[len + 1];
		for (index_int i = 0; i < len; i++)
			n[i] = list[i];
		n[len] = el;
		delete[]list;
		list = n;
		len++;
		return len;
	}

	index_int length() const
	{
		return len;
	}

	void foreach(void (*f)(T t))
	{
		for(index_int i =0;i < len; i++)
			f((*this)[i]);
	}
};

template <class T>
std::ostream &operator<<(std::ostream &out, Vector <T> const &v)
{
	out << "("<<v.length()<<")[";
	if (v.length() > 1)
	{
		index_int i = 0;
		for (; i < v.length() - 1; i++)
			out << v[i] << ", ";
		out << v[i];
	}
	else if (v.length() == 1)
		out << v[0];
	out << "]";
	return out;
}


#endif //CPP_VECTOR_HPP
