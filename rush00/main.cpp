//
// Created by AndreiMalcoci on 7/22/17.
//

#include <ncurses.h>
#include <string>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <iomanip>
#include "Game.hpp"
#include "Object.hpp"
#include "Wall.hpp"
#include "Logger.hpp"
#include "Unit.hpp"

int g_lvl = 1;
int g_bspeed = 3;
index_int g_units = 20;
WINDOW *mainWindow;
WINDOW *infoWindow;

Vector<Wall *> generateMap(WINDOW *w) {
    int mx = getmaxx(w);
    Vector<Wall *> map;
    for (int i = 0; i < mx; i++) {
        Wall *wall = new Wall(g_lvl);
        wall->setPos(w, i);
        map.push(wall);
    }
    return map;
}

void moveO(Object *o) {
    o->setX(o->getX() - 1);
}

void moveW(Wall *t) {
    t->getUp()->foreach(moveO);
    t->getDown()->foreach(moveO);
}

void updateMap(Vector<Wall *> *map, WINDOW *w, int lvl) {
    Wall *wall = map->pop((index_int) 0);
    delete wall;
    int mx = getmaxx(w);
    map->foreach(moveW);
    wall = new Wall(lvl);
    wall->setPos(w, mx - 1);
    map->push(wall);
}

void drawMap(Wall *w) {
    w->draw(mainWindow);
}

void gameLoop() {


    Vector<Wall *> map = generateMap(mainWindow);
    Unit *player = new Unit(L'u');
    player->setLive(6);
    player->setArmour(1);

    int my = getmaxy(mainWindow);
    int mx = getmaxx(mainWindow);
    player->setX(4);
    player->setY(my / 2);
    for (index_int i = 0; i < g_units; i++)
        Unit::randomUnit(mainWindow);
    int loop = 0;
    clock_t t = clock();
    clock_t l = clock();
    while (true) {

        if (true) {
            l = clock();
            int i = getch();

            g_bspeed += 0.1;
            wclear(mainWindow);
            wclear(infoWindow);
            player->collusion();


            if (player->getLive() <= 0)
                break;
            if (player->points % 100 == 99)
                player->setLive(player->getLive()+1);
            std::stringstream sout;;
            float timef = ((float) clock() - t) * 100 / CLOCKS_PER_SEC;
            int timei = (int) timef;

            sout << "Live: " << player->getLive() / 2 << " Time:" << std::setw(10) << std::setprecision(2) << timei;
            sout << " Points: " << std::setw(10) << player->points;

            mvwprintw(infoWindow, 0, 0, sout.str().c_str());
            sout << "objects:" << Object::getList()->length();
            Logger::main->log(LogFile, sout.str());
            player->draw(mainWindow);
            map.foreach(drawMap);
            Vector<Object *> *all = Object::getList();
            for (index_int i = 0; i < all->length(); i++) {
                Object *object = all->operator[](i);


                if (object->getType() == 1) {
                    Unit *unit = (Unit *) all->operator[](i);
                    if (unit != player) {
                        unit->move();
                        if ((rand() % 100) > 100 - g_bspeed) {
                            if (unit->getChar() == 'B') {
                                unit->fire()->setC('@');
                            }
                            else
                                unit->fire();
                        }
                        if( unit->points > 0 && unit->getChar() != 'B')
                        {
                            unit->setC('B');
                            unit->power = 2;
                            unit->setLive(1);
                            unit->setArmour(0);
                        }
                    }

                }

                if (object->getType() > 0 && object != player) {
                    object->collusion();
                    object->draw(mainWindow);
                    if (object->getX() <= 0 || object->getX() >= mx) {
                        delete object;
                    } else if (object->getLive() < 0)
                        delete object;
                }

            }

            if (Unit::units < g_units) {
                for (index_int i = Unit::units; i < g_units; i++) {
                    Unit::randomUnit(mainWindow);
                    player->points++;
                }
            }
            updateMap(&map, mainWindow, g_lvl);
            attron(COLOR_PAIR(2));
            player->draw(mainWindow);
            attron(COLOR_PAIR(1));
            loop++;
            wrefresh(mainWindow);
            wrefresh(infoWindow);
            if (i == UP_GAME)
                if (player->getY() > 0)
                    player->setY(player->getY() - 1);
            if (i == DOWN_GAME)
                if (player->getY() < my)
                    player->setY(player->getY() + 1);
            if (i == LEFT_GAME)
                if (player->getX() < mx)
                    player->setX(player->getX() + 1);
            if (i == RIGHT_GAME)
                if (player->getX() > 0)
                    player->setX(player->getX() - 1);
            if (i == ' ')
                player->fire()->setDirection(1)->setChar('>');
            if (i == 'w')
                g_units++;
            if (i == 'e')
                g_bspeed++;
            if (i == 's')
                g_lvl--;
            if (i == 'd')
            {
                if (g_units == 0)
                    g_units = 2;
                g_units--;
            }
            if (i == 'r') {
                if (g_bspeed == 0)
                    g_bspeed = 2;
                g_bspeed--;
            }
            if (i == 'Q' || i == 'q')
                break;
        }
    }
}


int main() {

    initscr(); // init ncurses
    int mx = getmaxx(stdscr);
    int my = getmaxy(stdscr);
    mainWindow = newwin(my - 1, mx, 1, 0);
    infoWindow = newwin(1, mx, 0, 0);
    Logger::main->log(LogFile, "Let's start");

    if (has_colors() == FALSE)
        return (0); //no colors on terminal
    start_color(); //define main composit

    init_color(COLOR_BLUE, 956, 529, 529);
    init_color(COLOR_MAGENTA, 1000, 974, 533);
    init_color(COLOR_CYAN, 416, 965, 686);
    init_pair(1, COLOR_BLACK, COLOR_RED); //Display Player
    init_pair(2, COLOR_BLACK, COLOR_YELLOW); //DISPLAY ENEMY UNIT
    init_pair(3, COLOR_BLACK, COLOR_GREEN); //DISPLAY ENEMY UNIT MISSILE
    init_pair(4, COLOR_RED, COLOR_BLUE); //DISPLAY OBSTACLE
    init_pair(5, COLOR_YELLOW, COLOR_MAGENTA); //DISPLAY PLAYER MISSILE
    timeout(100);
    curs_set(0);
    raw();

    gameLoop();
    delwin(infoWindow);
    delwin(mainWindow);
    endwin();


    return (0);
}
