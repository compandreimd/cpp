//
// Created by AndreiMalcoci on 7/23/17.
//

#include "Unit.hpp"

index_int Unit::units = 0;

Unit::Unit() : Object('O', 0, 0)
{
    live = 1;
    armour = 0;
    points = 0;
    power = 1;
    units++;
    drawed = 0;
    this->type = 1;
}

Unit::Unit(wchar_t c)
{
    setC(c);
    live = 1;
    armour = 0;
    power =1;
    points = 0;
    units++;
    drawed = 0;
    this->type = 1;
}

Unit::Unit(Unit const &u) {
    this->setX(u.getX());
    this->setY(u.getY());
    this->setC(u.getChar());
}

Unit& Unit::operator=(Unit const &u) {
    this->setX(u.getX());
    this->setY(u.getY());
    this->setC(u.getChar());
    return *this;
}

Unit::~Unit() {
    units--;
}


void Unit::move() {
    if(drawed > 10)
    {
        setX(getX()-1);
        drawed = 0;
    }
    //x = -1;
}

void Unit::draw(WINDOW *w) {
    mvwaddch(w, y,x, c);
    drawed++;
}

Bullet *Unit::fire()
{
    attron(3);
    return new Bullet(getX() - 2, getY(), this);
}

Unit* Unit::randomUnit(WINDOW *w) {
    int my = getmaxy(w);
    int mx = getmaxx(w);
    Unit *u = new Unit();
    u->setX( rand() % mx);
    u->setY(rand() % my);
    u->setLive(rand() % 5 + 1);
    return u;
}

bool Unit::onColusion(Object *object) {
    object->setLive(0);
    return true;
}

