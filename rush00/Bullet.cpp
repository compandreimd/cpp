//
// Created by AndreiMalcoci on 7/23/17.
//

#include "Bullet.hpp"
#include "Unit.hpp"

Bullet::Bullet(int x, int y, Unit *parent) : Object(L'<',0,0)
{
    direction = -1;
    live = 1;
    armour = 0;
    this->x = x - direction;
    this->y = y;
    this->parent = parent;
    this->type = 2;
}

bool Bullet::onColusion(Object *objec) {
    parent->points++;
    int dt = parent->power - objec->getArmour();
    if (dt > 0)
        objec->setLive(objec->getLive() - dt);
    return true;
}

void Bullet::draw(WINDOW *w) {
    attron(COLOR_PAIR(3));
    mvwaddch(w, getY(),getX(), getChar());
    setX(getX()+direction);
}

Bullet* Bullet::setDirection(int i) {
    setX(getX() + 2*i);
    direction = i;
    return this;
}


Bullet* Bullet::setChar(char c) {
    this->setC(c);
    return this;
}

