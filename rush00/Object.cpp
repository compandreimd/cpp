//
// Created by AndreiMalcoci on 7/22/17.
//

#include "Object.hpp"
#include "Unit.hpp"

Vector<Object*> *Object::list = new Vector<Object*>();

Object::Object()
{
    this->c = L'#';
    this->x = 0;
    this->y = 0;
    this->type = 0;
    this->live = INT_MAX;
    this->armour = INT_MAX;
    list->push(this);
}

Object::Object(wchar_t c, int x, int y)
{
    this->c = c;
    this->x = x;
    this->y = y;
    this->type = 0;
    this->live = INT_MAX;
    this->armour = INT_MAX;
    list->push(this);
}


Object::Object(Object const &obj) {
    this->c = obj.c;
    this->x = obj.x;
    this->y = obj.y;
}

Object &Object::operator=(Object const &obj) {
    this->c = obj.c;
    this->x = obj.x;
    this->y = obj.y;
    return *this;
}

int Object::getX() const {
    return x;
}

int Object::getY() const {
    return y;
}

wchar_t Object::getChar() const {
    return c;
}

Object* Object::setX(int x) {
    this->x = x;
    return  this;
}

Object* Object::setY(int y) {
    this->y = y;
    return  this;
}

Object* Object::setC(wchar_t c) {
    this->c = c;
    return  this;
}

Object::~Object() {
    list->pop(this);
}

Vector <Object *> *Object::getList()
{
    return list;
}

std::ostream &operator<<(std::ostream &out,Object const &ob)
{
    out << "Object(" << ob.getChar() << ", " << ob.getX() << ", " << ob.getY() << ")";
	return out;
}

int Object::getLive() const {
    return  live;
}

int Object::getArmour() const {
    return armour;
}

int Object::getType() const {
    return type;
}

Object* Object::setLive(int live) {
    this->live = live;
    return this;
}

Object* Object::setArmour(int armour) {
    this->armour= armour;
    return this;
}


void Object::draw(WINDOW *window)
{
	mvwaddch(window, y,x, c);
}

bool Object::onColusion(Object *object) {
    object->live = 0;
    return true;
}

bool Object::collusion() {
    Vector<Object *> *list = Object::getList();
    for(index_int i = 0; i < list->length(); i++)
    {
        if (list->operator[](i) != this)
        {
            Object *obj = list->operator[](i);
            if (obj->getX() == getX() && obj->getY() == getY())
            {
                return obj->onColusion(this);
            }
        }
    }
    return false;
}