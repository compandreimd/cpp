#include "PowerFist.hpp"

/*
** constructor
*/
PowerFist::PowerFist(void) : AWeapon("Power Fist", 8, 50)
{

}

PowerFist::PowerFist(PowerFist const &copy) : AWeapon(copy)
{
	
}

/*
** destructor
*/
PowerFist::~PowerFist(void)
{

}

/*
** operator overload
*/
PowerFist				&PowerFist::operator=(PowerFist const &copy)
{
	AWeapon::operator=(copy);

	return *this;
}


/*
** public
*/
void					PowerFist::attack(void) const
{
	std::cout << "* pschhh... SBAM! *" << std::endl;
}
