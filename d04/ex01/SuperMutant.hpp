#ifndef SUPERMUTANT_HPP

/*
** defines
*/
# define SUPERMUTANT_HPP

/*
** includes
*/
# include "Enemy.hpp"

/*
** class
*/
class SuperMutant : public Enemy
{
	public:
		SuperMutant(void);
		SuperMutant(SuperMutant const &copy);
		virtual ~SuperMutant(void);

		void			takeDamage(int damage);
		SuperMutant		&operator=(SuperMutant const &copy);

	private:

};

#endif //SUPERMUTANT_HPP
