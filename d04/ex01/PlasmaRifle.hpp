#ifndef PLASMARIFLE_HPP

/*
** defines
*/
# define PLASMARIFLE_HPP

/*
** includes
*/
# include "AWeapon.hpp"

/*
** class
*/
class PlasmaRifle : public AWeapon
{
	public:
		PlasmaRifle(void);
		PlasmaRifle(PlasmaRifle const &copy);
		virtual ~PlasmaRifle(void);

		void			attack(void) const;
		PlasmaRifle		&operator=(PlasmaRifle const &copy);

	private:
		
};

#endif //PLASMARIFLE_HPP
