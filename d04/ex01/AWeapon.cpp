#include "AWeapon.hpp"

/*
** constructor
*/
AWeapon::AWeapon(void)
{

}

AWeapon::AWeapon(std::string const &name, int apcost, int damage) :
	_name(name), _apcost(apcost), _damage(damage)
{

}

AWeapon::AWeapon(AWeapon const &copy)
{
	this->_name = copy._name;
	this->_apcost = copy._apcost;
	this->_damage = copy._damage;
}

/*
** destructor
*/
AWeapon::~AWeapon(void)
{

}

/*
** operator overload
*/
AWeapon				&AWeapon::operator=(AWeapon const &rhs)
{
	this->_name = rhs._name;
	this->_apcost = rhs._apcost;
	this->_damage = rhs._damage;

	return *this;
}

/*
** getter/setter
*/
std::string const	&AWeapon::getName(void) const	{ return this->_name; }
int					AWeapon::getAPCost(void) const	{ return this->_apcost; }
int					AWeapon::getDamage(void) const	{ return this->_damage; }
