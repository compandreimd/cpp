#ifndef ENEMY_HPP

/*
** defines
*/
# define ENEMY_HPP

/*
** includes
*/
# include <iostream>

/*
** class
*/
class Enemy
{
	public:
		Enemy(void);
		Enemy(int hp, std::string const &type);
		Enemy(Enemy const &copy);
		virtual ~Enemy(void);

		std::string const	&getType(void) const;
		int					getHP(void) const;
		virtual void		takeDamage(int damage);

		Enemy				&operator=(Enemy const &copy);

	private:
		int				_hp;
		std::string		_type;

};

#endif //ENEMY_HPP
