#ifndef CHARACTER_HPP
# define CHARACTER_HPP


# include <iostream>
# include "AWeapon.hpp"
# include "Enemy.hpp"

class Character
{
	public:
		Character(void);
		Character(std::string const &name);
		Character(Character const &copy);
		~Character(void);

		std::string const	&getName(void) const;
		int					getAp(void)	const;
		AWeapon				*getWeapon(void)	const;

		void				recoverAP(void);
		void				equip(AWeapon *weapon);
		void				attack(Enemy *enemy);

		Character			&operator=(Character const &copy);

	private:
		std::string			_name;
		int					_ap;
		AWeapon				*_weapon;

};


std::ostream				&operator<<(std::ostream &o, Character const &rhs);

#endif //CHARACTER_HPP
