#ifndef POWERFIST_HPP

/*
** defines
*/
# define POWERFIST_HPP

/*
** includes
*/
# include "AWeapon.hpp"

/*
** class
*/
class PowerFist : public AWeapon
{
	public:
        PowerFist(void);
        PowerFist(PowerFist const &copy);
        virtual ~PowerFist(void);

        void            attack(void) const;
        PowerFist     &operator=(PowerFist const &copy);

	private:

};

#endif //POWERFIST_HPP
