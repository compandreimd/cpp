#ifndef RADSCORPION_HPP

/*
** defines
*/
# define RADSCORPION_HPP

/*
** includes
*/
# include "Enemy.hpp"

/*
** class
*/
class RadScorpion : public Enemy
{
	public:
		RadScorpion(void);
		RadScorpion(RadScorpion const &copy);
		virtual ~RadScorpion(void);

		using			Enemy::takeDamage;
		RadScorpion		&operator=(RadScorpion const &copy);

	private:

};

#endif //RADSCORPION_HPP
