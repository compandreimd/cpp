//
// Created by AndreiMalcoci on 7/27/17.
//

#include <iostream>
#include "Sorcerer.hpp"


Sorcerer::Sorcerer() {

}

Sorcerer::Sorcerer(std::string const &name, std::string const &title) {
    this->name = name;
    this->title = title;
    std::cout << name << ", " << title << ", is born !" << std::endl;
}

Sorcerer::Sorcerer(Sorcerer const &s) {
    this->name = s.name;
    this->title = s.title;
}

Sorcerer& Sorcerer::operator=(Sorcerer const &s) {
    this->name = s.name;
    this->title = s.title;
    return *this;
}

Sorcerer::~Sorcerer() {
    std::cout << name << ", " << title << ", is dead. Consequences will never be the same !" << std::endl;
}

void Sorcerer::polymorph(Victim const &v) const {
    v.getPolymorphed();
}

std::ostream &operator<<(std::ostream &out, Sorcerer const &s)
{
    out << "I am " << s.name << ", " << s.title << ", and I like ponies !" << std::endl;
    return out;
}