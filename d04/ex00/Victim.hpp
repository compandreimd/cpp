//
// Created by AndreiMalcoci on 7/27/17.
//

#ifndef CPP_VICTIM_HPP
#define CPP_VICTIM_HPP


#include <string>

class Victim {
private:
    Victim();
public:
    std::string name;
    Victim(std::string const &name);
    Victim(Victim const &v);
    Victim &operator=(Victim const &v);
    ~Victim();
    virtual void getPolymorphed() const;
};

std::ostream &operator<<(std::ostream &out, Victim const &v);


#endif //CPP_VICTIM_HPP
