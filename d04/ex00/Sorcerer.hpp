//
// Created by AndreiMalcoci on 7/27/17.
//

#ifndef CPP_SORCERER_HPP
#define CPP_SORCERER_HPP


#include <string>
#include "Victim.hpp"

class Sorcerer {

    Sorcerer();
public:
    std::string name;
    std::string title;
    Sorcerer(std::string const &name, std::string const &title);
    Sorcerer(Sorcerer const &s);
    Sorcerer &operator=(Sorcerer const &s);
    ~Sorcerer();
    void polymorph(Victim const &v) const;
};

std::ostream &operator<<(std::ostream &out, Sorcerer const &sorcerer);

#endif //CPP_SORCERER_HPP
