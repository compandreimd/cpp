//
// Created by AndreiMalcoci on 7/27/17.
//

#include <iostream>
#include "Peon.hpp"

Peon::Peon() :Victim("Peon") {

}

Peon::Peon(std::string const &name) : Victim(name)
{
    std::cout << "Zog zog." << std::endl;
}

Peon::Peon(Peon const &v): Victim(v){

}

Peon& Peon::operator=(Peon const &v) {
    this->name = v.name;
    return *this;
}

Peon::~Peon() {
    std::cout << "Bleuark..." << std::endl;
}

void Peon::getPolymorphed() const {
    std::cout << name << " has been turned into a pink pony !" << std::endl;
}