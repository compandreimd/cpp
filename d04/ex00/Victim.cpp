//
// Created by AndreiMalcoci on 7/27/17.
//

#include <iostream>
#include "Victim.hpp"

Victim::Victim() {

}

Victim::Victim(std::string const &name) {
    this->name = name;
    std::cout << "Some random victim called " << name << " just popped !" << std::endl;
}

Victim::Victim(Victim const &v) {
    this->name = v.name;
}

Victim& Victim::operator=(Victim const &v) {
    this->name = v.name;
    return *this;
}

Victim::~Victim() {
    std::cout << "Victim " << name << " just died for no apparent reason !" << std::endl;
}

std::ostream &operator<<(std::ostream &out, Victim const &v)
{
    out << "I'm "<< v.name << " and I like otters !" << std::endl;
    return out;
}

void Victim::getPolymorphed() const {
    std::cout << name << " has been turned into a cute little sheep !" << std::endl;
}