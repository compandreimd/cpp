//
// Created by AndreiMalcoci on 7/27/17.
//

#ifndef CPP_PEON_HPP
#define CPP_PEON_HPP


#include <string>
#include "Victim.hpp"

class Peon: public Victim
{
    Peon();
public:
    Peon(std::string const &name);
    Peon(Peon const &p);
    Peon &operator=(Peon const &p);
    ~Peon();
    void getPolymorphed() const;

};



#endif //CPP_PEON_HPP
