// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Cure.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:25:40 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:25:44 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include "Cure.hpp"
#include "ICharacter.hpp"

Cure::Cure(void) : AMateria("cure")
{

}

Cure::Cure(Cure const &cure) : AMateria(cure.getType())
{

}

Cure Cure::operator=(Cure const &cure) {
    ((AMateria*) this)->operator=(cure);
    return *this;
}


Cure::~Cure(void)
{

}

void Cure::use(ICharacter &target) {
    AMateria::use(target);
    std::cout << "* heals " << target.getName() << "’s wounds *" << std::endl;
}

AMateria* Cure::clone() const {
    return new Cure(*this);
}