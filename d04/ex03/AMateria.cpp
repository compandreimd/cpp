// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   AMateria.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:24:48 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:24:52 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "AMateria.hpp"

AMateria::AMateria(void)
{

}

AMateria::AMateria(std::string const &type)
{
    this->type = type;
    this->xp_ = 0;
}

AMateria::~AMateria(void)
{

}

std::string const& AMateria::getType() const {
    return  this->type;
}

unsigned  AMateria::getXP() const {
    return  this->xp_;
}


void AMateria::use(ICharacter &target) {
    this->xp_ += 10;
    (void)target;
}
