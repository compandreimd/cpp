// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Cure.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:25:29 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:25:35 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef CURE_HPP

# define CURE_HPP


#include "AMateria.hpp"

class Cure : public AMateria
{
public:
	Cure(void);
	Cure(Cure const &cure);
	Cure operator=(Cure const &cure);
	~Cure(void);
	void use(ICharacter& target);
	AMateria* clone() const;
};

#endif //CURE_HPP
