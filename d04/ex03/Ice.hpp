// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Ice.hpp                                            :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:25:03 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:25:09 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ICE_HPP

# define ICE_HPP

#include "AMateria.hpp"

class Ice : public AMateria
{
public:
	Ice(void);
	Ice(Ice const &ice);
	Ice operator=(Ice const &ice);
	~Ice(void);
	void use(ICharacter& target);
	AMateria* clone() const;
};

#endif //ICE_HPP
