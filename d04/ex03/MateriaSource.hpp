// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   MateriaSource.hpp                                  :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:26:47 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:26:53 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef MATERIASOURCE_HPP


# define MATERIASOURCE_HPP



#include "IMateriaSource.hpp"


class MateriaSource :public IMateriaSource
{
public:
	MateriaSource(void);
	MateriaSource(MateriaSource const &materiaSource);
	MateriaSource operator=(MateriaSource const &materiaSource);
	~MateriaSource(void);
	void learnMateria(AMateria*);
	AMateria* createMateria(std::string const & type);
private:
	AMateria *materials[4];
	int l;
};

#endif //MATERIASOURCE_HPP
