// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Ice.cpp                                            :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:25:15 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:25:20 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include "Ice.hpp"
#include "ICharacter.hpp"

/*
** constructor
*/
Ice::Ice(void) : AMateria("ice")
{

}

Ice::Ice(Ice const &ice) : AMateria(ice.getType())
{

}

Ice Ice::operator=(Ice const &ice) {
    ((AMateria*) this)->operator=(ice);
    return *this;
}


Ice::~Ice(void)
{

}

void Ice::use(ICharacter &target) {
    AMateria::use(target);
    std::cout << "* shoots an ice bolt at " << target.getName() << " *" << std::endl;
}

AMateria* Ice::clone() const {
    return new Ice(*this);
}