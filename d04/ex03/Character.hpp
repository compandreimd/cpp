// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Character.hpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:07:00 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:26:27 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef CHARACTER_HPP

# define CHARACTER_HPP


#include <string>
#include "ICharacter.hpp"
#include "AMateria.hpp"

class Character : public ICharacter
{
	Character();
	std::string name;
	AMateria* list[4];
	int l;
public:
	Character(std::string const &name);
	Character(Character const & character);
	Character &operator=(Character const &character);
	~Character();
	std::string const & getName() const;
	void	 equip(AMateria* m);
	void	 unequip(int idx);
	void	 use(int idx, ICharacter& target);
};
#endif //CHARACTER_HPP
