// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   MateriaSource.cpp                                  :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:26:58 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:27:03 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "MateriaSource.hpp"

MateriaSource::MateriaSource(void)
{
    l = 0;
}

MateriaSource::MateriaSource(MateriaSource const &materiaSource) {
    this->l = materiaSource.l;
    for(int i = 0; i < l; i++) {
        this->materials[l] = materiaSource.materials[l];
    }
}

MateriaSource MateriaSource::operator=(MateriaSource const &materiaSource) {
    this->l = materiaSource.l;
    for(int i = 0; i < l; i++) {
        this->materials[l] = materiaSource.materials[l];
    }
    return *this;
}

MateriaSource::~MateriaSource(void)
{
}

AMateria* MateriaSource::createMateria(std::string const &type) {
    for(int i = 0; i < l; i++)
    {
        if(materials[i]->getType() == type)
        {
            return materials[i]->clone();
        }
    }
    return nullptr;
}

void MateriaSource::learnMateria(AMateria *materia) {
    materials[l] = materia;
    l++;
}
