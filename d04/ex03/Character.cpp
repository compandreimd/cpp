// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Character.cpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:08:00 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:08:15 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Character.hpp"

Character::Character() {}

Character::Character(std::string const &name) {
    this->name = name;
    l = 0;
}

Character::Character(Character const &character) {
    this->name = character.name;
    this->l = character.l;
    for(int i = 0; i < character.l;i++)
        this->list[i] = character.list[i];
}

Character& Character::operator=(Character const &character) {
    this->name = character.name;
    this->l = character.l;
    for(int i = 0; i < character.l;i++)
        this->list[i] = character.list[i];
    return *this;
}

Character::~Character() {}

std::string const& Character::getName() const {
    return this->name;
}

void Character::equip(AMateria *m) {
    if(l < 4)
    {
        this->list[l] = m;
        l++;
    }
}

void Character::unequip(int idx) {
    l-=idx;
}

void Character::use(int idx, ICharacter &target) {
    list[idx]->use(target);
}