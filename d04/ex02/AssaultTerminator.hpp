#ifndef ASSAULTTERMINATOR_HPP

/*
** defines
*/
# define ASSAULTTERMINATOR_HPP

/*
** includes
*/
# include "ISpaceMarine.hpp"

/*
** class
*/
class AssaultTerminator : public ISpaceMarine
{
	public:
		AssaultTerminator(void);
		AssaultTerminator(AssaultTerminator const &copy);
		~AssaultTerminator(void);

		AssaultTerminator		&operator=(AssaultTerminator const &copy);
		AssaultTerminator		*clone(void) const;
		void					battleCry(void) const;
		void					rangedAttack(void) const;
		void					meleeAttack(void) const;

	private:

};

#endif //ASSAULTTERMINATOR_HPP
