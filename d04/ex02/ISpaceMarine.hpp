#ifndef ISPACEMARINE_HPP

/*
** defines
*/
# define ISPACEMARINE_HPP

/*
** includes
*/
# include <iostream>

/*
** class
*/
class ISpaceMarine
{
	public:
		virtual	~ISpaceMarine(void) {};

		virtual ISpaceMarine	*clone(void) const = 0;
		virtual	void			battleCry(void) const = 0;
		virtual	void			rangedAttack(void) const = 0;
		virtual	void			meleeAttack(void) const = 0;
};

#endif //ISPACEMARINE_HPP
