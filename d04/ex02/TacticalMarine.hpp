#ifndef TACTICALMARINE_HPP

/*
** defines
*/
# define TACTICALMARINE_HPP

/*
** includes
*/
# include "ISpaceMarine.hpp"

/*
** class
*/
class TacticalMarine : public ISpaceMarine
{
	public:
		TacticalMarine(void);
		TacticalMarine(TacticalMarine const &copy);
		~TacticalMarine(void);

		TacticalMarine		&operator=(TacticalMarine const &copy);
		TacticalMarine		*clone(void) const;
		void				battleCry(void) const;
		void				rangedAttack(void) const;
		void				meleeAttack(void) const;

	private:

};

#endif //TACTICALMARINE_HPP
