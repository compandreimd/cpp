// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   StripMiner.hpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:33:59 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:34:04 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef STRIPMINER_HPP

/*
** defines
*/
# define STRIPMINER_HPP

/*
** includes
*/
//# include <iostream>

#include "IMiningLaser.hpp"

/*
** class
*/
class StripMiner :public  IMiningLaser
{
public:
	StripMiner(void);
	StripMiner(StripMiner const &d);
	StripMiner&operator=(StripMiner const &d);
	~StripMiner(void);
	void mine(IAsteroid*);



};

#endif //STRIPMINER_HPP
