// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   StripMiner.cpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:34:10 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:34:13 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include "StripMiner.hpp"
#include "IAsteroid.hpp"

StripMiner::StripMiner() {}

StripMiner::StripMiner(StripMiner const &d) {
    (void)d;
}

StripMiner& StripMiner::operator=(StripMiner const &d) {
    (void)d;
    return *this;
}

StripMiner::~StripMiner() {}

void StripMiner::mine(IAsteroid *asteroid) {
    std::cout << "* strip mining ... got "<<  asteroid->beMined(this) << " ! *" << std::endl;
}