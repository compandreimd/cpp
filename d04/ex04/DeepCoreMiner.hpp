// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   DeepCoreMiner.hpp                                  :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:33:32 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:33:38 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef DEEPCOREMINER_HPP

/*
** defines
*/
# define DEEPCOREMINER_HPP

/*
** includes
*/
//# include <iostream>

#include "IMiningLaser.hpp"

/*
** class
*/
class DeepCoreMiner : public IMiningLaser
{
public:
	DeepCoreMiner(void);
	DeepCoreMiner(DeepCoreMiner const &d);
	DeepCoreMiner&operator=(DeepCoreMiner const &d);
	~DeepCoreMiner(void);
	void mine(IAsteroid*);

};

#endif //DEEPCOREMINER_HPP
