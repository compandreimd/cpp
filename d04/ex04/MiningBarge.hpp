// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   MiningBarge.hpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:35:23 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:37:23 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef MININGBARGE_HPP

#include "IMiningLaser.hpp"

class MiningBarge
{
private:
	int l;
	IMiningLaser* list[4];
public:
	MiningBarge();
	MiningBarge(MiningBarge const &barge);
	MiningBarge&operator=(MiningBarge const &barge);
	~MiningBarge();
	void equip(IMiningLaser*);
	void mine(IAsteroid*) const;
};

#endif //MININGBARGE_HPP
