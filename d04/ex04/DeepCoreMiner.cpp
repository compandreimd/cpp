// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   DeepCoreMiner.cpp                                  :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:33:44 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:33:48 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include "DeepCoreMiner.hpp"
#include "IAsteroid.hpp"

DeepCoreMiner::DeepCoreMiner() {}

DeepCoreMiner::DeepCoreMiner(DeepCoreMiner const &d) {
    (void)d;
}

DeepCoreMiner& DeepCoreMiner::operator=(DeepCoreMiner const &d) {
    (void)d;
    return *this;
}

DeepCoreMiner::~DeepCoreMiner() {}

void DeepCoreMiner::mine(IAsteroid *asteroid) {
    std::cout << "* mining deep ... got "<<  asteroid->beMined(this) << " ! *" << std::endl;
}