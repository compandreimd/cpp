//
// Created by AndreiMalcoci on 7/29/17.
//

#ifndef CPP_ASTEROBOCAL_HPP
#define CPP_ASTEROBOCAL_HPP


#include "IAsteroid.hpp"
#include "DeepCoreMiner.hpp"
#include "StripMiner.hpp"

class AsteroKreog : public IAsteroid
{

public:
    AsteroKreog();
    AsteroKreog(AsteroKreog const &asteroBocal);
    AsteroKreog&operator=(AsteroKreog const &asteroBocal);
    ~AsteroKreog();
    std::string beMined(IMiningLaser *) const;
    std::string beMined(DeepCoreMiner *) const;
    std::string beMined(StripMiner *) const;
    std::string getName() const;
};


#endif //CPP_ASTEROBOCAL_HPP
