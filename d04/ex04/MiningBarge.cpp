// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   MiningBarge.cpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:35:37 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:35:41 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "MiningBarge.hpp"


MiningBarge::MiningBarge() {
    l = 0;
}

MiningBarge::MiningBarge(MiningBarge const &barge) {
    this->l = barge.l;
    for(int i = 0 ; i < l; i++)
    {
        this->list[i] = barge.list[i];
    }
}

MiningBarge& MiningBarge::operator=(MiningBarge const &barge) {
    this->l = barge.l;
    for(int i = 0 ; i < l; i++)
    {
        this->list[i] = barge.list[i];
    }
    return *this;
}


MiningBarge::~MiningBarge() {

}

void MiningBarge::equip(IMiningLaser *miningLaser) {
    if (l < 4) {
        this->list[l] = miningLaser;
        l++;
    }
}

void MiningBarge::mine(IAsteroid *asteroid) const {
    for(int i = 0 ; i < l; i++)
        this->list[i]->mine(asteroid);
}