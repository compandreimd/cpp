// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   IAsteroid.hpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:35:54 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:37:04 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef IASTEROID_HPP


# define IASTEROID_HPP

/*
** includes
*/
//# include <iostream>

#include <string>
#include "IMiningLaser.hpp"
#include "DeepCoreMiner.hpp"
#include "StripMiner.hpp"


/*
** class
*/

class IAsteroid
{
public:
	virtual ~IAsteroid() {}
	virtual std::string beMined(IMiningLaser *) const = 0;
	virtual std::string beMined(DeepCoreMiner *) const = 0;
	virtual std::string beMined(StripMiner *) const = 0;
	virtual std::string getName() const = 0;

};

#endif //IASTEROID_HPP
