//
// Created by AndreiMalcoci on 7/29/17.
//

#include "KoalaSteroid.hpp"

KoalaSteroid::KoalaSteroid() {}

KoalaSteroid::KoalaSteroid(KoalaSteroid const &asteroBocal) {
    (void) asteroBocal;
}

KoalaSteroid& KoalaSteroid::operator=(KoalaSteroid const &asteroBocal) {
    (void) asteroBocal;
    return *this;
}

KoalaSteroid::~KoalaSteroid() {}

std::string KoalaSteroid::getName() const
{
    return "BocalSreroid";
}
std::string KoalaSteroid::beMined(IMiningLaser *l) const{
    (void) l;
    return "A";
}

std::string KoalaSteroid::beMined(StripMiner *l) const{
    (void) l;
    return "Krpite";
}

std::string KoalaSteroid::beMined(DeepCoreMiner *l) const{
    (void) l;
    return "Zazium";
}
