// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: mcanal <zboub@42.fr>                       +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 00:33:02 by mcanal            #+#    #+#             //
//   Updated: 2015/06/18 00:33:06 by mcanal           ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "MiningBarge.hpp"
#include "DeepCoreMiner.hpp"
#include "AsteroKreog.hpp"
#include "KoalaSteroid.hpp"

int		main(void)
{
	MiningBarge *miningBarge=new MiningBarge;
	miningBarge->equip(new DeepCoreMiner());
	miningBarge->equip(new StripMiner());
	miningBarge->mine(new AsteroKreog());
	miningBarge->mine(new KoalaSteroid());
	return (0);
}
