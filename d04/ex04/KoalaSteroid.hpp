//
// Created by AndreiMalcoci on 7/29/17.
//

#ifndef CPP_BOCALSTEROID_HPP
#define CPP_BOCALSTEROID_HPP


#include "IAsteroid.hpp"
#include "DeepCoreMiner.hpp"
#include "StripMiner.hpp"

class KoalaSteroid : public IAsteroid {
public:
    KoalaSteroid();
    KoalaSteroid(KoalaSteroid const &asteroBocal);
    KoalaSteroid&operator=(KoalaSteroid const &asteroBocal);
    ~KoalaSteroid();
    std::string beMined(IMiningLaser *) const;
    std::string beMined(DeepCoreMiner *) const;
    std::string beMined(StripMiner *) const;
    std::string getName() const;
};


#endif //CPP_BOCALSTEROID_HPP
