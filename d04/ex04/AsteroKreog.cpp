//
// Created by AndreiMalcoci on 7/29/17.
//

#include "AsteroKreog.hpp"
#include "StripMiner.hpp"
#include "IMiningLaser.hpp"

AsteroKreog::AsteroKreog() {}

AsteroKreog::AsteroKreog(AsteroKreog const &asteroBocal) {
    (void) asteroBocal;
}

AsteroKreog& AsteroKreog::operator=(AsteroKreog const &asteroBocal) {
    (void) asteroBocal;
    return *this;
}

AsteroKreog::~AsteroKreog() {}

std::string AsteroKreog::getName() const
{
    return "AstroBocal";
}


std::string AsteroKreog::beMined(IMiningLaser *l) const{
    (void) l;
    return "XD";
}

std::string AsteroKreog::beMined(StripMiner *l) const{
    (void) l;
    return "Flavium";
}

std::string AsteroKreog::beMined(DeepCoreMiner *l) const{
    (void) l;
    return "Thorite";
}
