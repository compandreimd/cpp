/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 14:04:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 14:04:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "Brain.hpp"
#include <sstream>


std::string Brain::identify() const {
    std::stringstream sout;
    sout << std::uppercase << this;
    return sout.str();
}