/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 14:05:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 14:05:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "Human.hpp"

const Brain &Human::getBrain() const {
    return brain;
}

std::string Human::identify() const {
    return brain.identify();
}