/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 14:04:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 14:04:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef CPP_BRAIN_HPP
#define CPP_BRAIN_HPP


#include <string>

class Brain {
public:
    std::string identify() const;
};


#endif //CPP_BRAIN_HPP
