/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 14:05:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 14:05:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef CPP_HUMAN_HPP
#define CPP_HUMAN_HPP


#include "Brain.hpp"

class Human {
    Brain brain;
public:
    const Brain &getBrain() const;

    std::string identify() const;
};


#endif //CPP_HUMAN_HPP
