/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 16:32:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 16:32:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string>
#include "Logger.h"

int main()
{
	Logger logger("test.log");
	logger.log(LogConsole, "Hi");
	logger.log(LogFile, "Secrete");
	logger.log(Log, "Bye");
}