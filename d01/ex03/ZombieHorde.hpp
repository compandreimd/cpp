/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 13:02:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 13:02:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef CPP_ZOMBIEHORDE_HPP
#define CPP_ZOMBIEHORDE_HPP


#include "Zombie.hpp"

class ZombieHorde {
    Zombie *list;
    int n;
public:
    ZombieHorde(int n);

    ~ZombieHorde();

    void announce() const;
};


#endif //CPP_ZOMBIEHORDE_HPP
