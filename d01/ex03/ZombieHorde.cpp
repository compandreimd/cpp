/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 13:02:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 13:02:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(int n) {
    this->n = n;
    list = new Zombie[n];
}

ZombieHorde::~ZombieHorde() {
    delete[]list;
}

void ZombieHorde::announce() const {
    for (int i = 0; i < n; i++)
        list[i].announce();
}