/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 16:31:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 16:31:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef CPP_HUMAN_HPP
#define CPP_HUMAN_HPP

#include <string>

class Human {
private:
    void meleeAttack(std::string const &target) const;

    void rangedAttack(std::string const &target) const;

    void intimidatingShout(std::string const &target) const;

    void (Human::*list_f[3])(std::string const &) const;

    std::string names[3];

public:
    void action(std::string const &action_name, std::string const &target) const;

    Human();

    const std::string *getNames() const;
};

#endif //CPP_HUMAN_HPP
