/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 16:31:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 16:31:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <iostream>
#include <typeinfo>
#include "Human.hpp"

void Human::intimidatingShout(std::string const &target) const {
    std::cout << "Intimidating Shout To " << target << std::endl;
}

void Human::meleeAttack(std::string const &target) const {
    std::cout << "Meal Attack To " << target << std::endl;
}

void Human::rangedAttack(std::string const &target) const {
    std::cout << "Ranged Attack To " << target << std::endl;
}

Human::Human() {
    list_f[0] = &Human::intimidatingShout;
    names[0] = "intimidatingShout";
    list_f[1] = &Human::meleeAttack;
    names[1] = "meleeAttack";
    list_f[2] = &Human::rangedAttack;
    names[2] = "rangedAttack";
}

void Human::action(std::string const &action_name, std::string const &target) const {
    for (int i = 0; i < 3; i++) {
        if (!action_name.compare(names[i]))
            (this->*list_f[i])(target);
    }
}

const std::string *Human::getNames() const {
    return names;
}
