/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 16:32:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 16:32:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string>
#include "Human.hpp"

int main() {
    Human human;
    const std::string *names = human.getNames();
    for (int i = 0; i < 3; i++)
        human.action(names[i], "AAA");
}