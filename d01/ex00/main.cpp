/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/18 11:20:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/18 11:20:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

void ponyOnTheHeap() {
    Pony *p = new Pony("Anabeli", 0xFFFFFF);
    p->status = "Heap";
    p->print();
    delete p;
}

void ponyOnTheStack() {
    Pony p("Anabeli", 0xFFFFFF);
    p.status = "Stack";
    p.print();
}

int main() {
    Pony test("Anabeli", 0xFFFFFF);
    test.print();
    ponyOnTheHeap();
    ponyOnTheStack();
    return 0;
}