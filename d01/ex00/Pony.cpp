/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/18 11:19:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/18 11:19:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "Pony.hpp"
#include <iostream>


Pony::Pony(std::string name, int color) {
    this->name = name;
    this->RGB = color;
    this->status = "UNKNOWN";
}


void Pony::print() const {
    std::cout << "Name:  " << this->name << std::endl;
    std::cout << "Color: " << std::hex << this->RGB << std::endl;
    std::cout << "Status: " << this->status << std::endl;
}
