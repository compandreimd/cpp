/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/18 11:19:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/18 11:19:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef CPP_PONY_HPP
#define CPP_PONY_HPP

#include <string>

class Pony {
private:
    int RGB;

    std::string name;

public:

    std::string status;

    Pony(std::string name, int color);

    void print() const;
};

#endif //CPP_PONY_HPP
