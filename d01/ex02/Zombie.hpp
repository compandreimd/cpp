/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 12:08:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 12:08:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef CPP_ZOMBIE_HPP
#define CPP_ZOMBIE_HPP


#include <string>

class Zombie {
private:
    std::string type;

    std::string name;

public:
    Zombie(std::string name, std::string type);

    void announce() const;
};


#endif //CPP_ZOMBIE_HPP
