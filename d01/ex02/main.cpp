//
// Created by root on 7/19/17
#include "ZombieEvent.hpp"

int main() {
    ZombieEvent event;
    Zombie *z = event.randomChump();
    delete (z);

    for (int i = 0; i < 10; i++) {
        event.setZombieType(std::to_string(i % 3));
        z = event.randomChump();
        delete (z);
    }

    event.setZombieType("A");
    z = event.randomChump();
    delete (z);
}
