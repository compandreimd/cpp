/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 12:25:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 12:25:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef CPP_ZOMBIEEVENT_HPP
#define CPP_ZOMBIEEVENT_HPP


#include <string>
#include "Zombie.hpp"

class ZombieEvent {
private:
    std::string zombieType;

public:
    void setZombieType(std::string zombieType);

    Zombie *newZoombie(std::string name) const;

    Zombie *randomChump() const;
};


#endif //CPP_ZOMBIEEVENT_HPP
