/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 12:25:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 12:25:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "ZombieEvent.hpp"
#include <string>

void ZombieEvent::setZombieType(std::string zombieType) {
    this->zombieType = zombieType;
}

Zombie *ZombieEvent::newZoombie(std::string name) const {
    return new Zombie(name, zombieType);
}

Zombie *ZombieEvent::randomChump() const {
    static unsigned i = 0;
    i++;
    Zombie *zombie = newZoombie("Zombie" + std::to_string(i));
    zombie->announce();
    return zombie;
}