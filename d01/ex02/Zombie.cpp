/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 12:08:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 12:08:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <iostream>
#include "Zombie.hpp"

Zombie::Zombie(std::string name, std::string type) {
    this->name = name;
    this->type = type;
}

void Zombie::announce() const {
    std::cout << "<" << name << " (" << type << ")> Braiiiiiiinnnssss..." << std::endl;
}