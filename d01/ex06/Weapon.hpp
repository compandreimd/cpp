/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 14:40:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 14:40:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef CPP_WEAPON_HPP
#define CPP_WEAPON_HPP


#include <string>

class Weapon {
    std::string type;
public:
    Weapon();

    Weapon(std::string type);

    const std::string &getType() const;

    void setType(const std::string &type);
};


#endif //CPP_WEAPON_HPP
