/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 14:49:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 14:49:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "HumanA.hpp"
#include <iostream>

HumanA::HumanA(std::string name, Weapon &weapon) {
    this->name = name;
    this->weapon = &weapon;
}

void HumanA::attack() {
    std::cout << name << " attacks with his " << weapon->getType() << std::endl;
}