/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 14:50:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 14:50:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef CPP_HUMANB_HPP
#define CPP_HUMANB_HPP


#include <string>
#include "Weapon.hpp"

class HumanB {
    Weapon *weapon;
    std::string name;
public:
    HumanB(std::string name);

    void attack();

    void setWeapon(Weapon &weapon);
};


#endif //CPP_HUMANB_HPP
