/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 14:49:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 14:49:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef CPP_HUMANA_HPP
#define CPP_HUMANA_HPP


#include "Weapon.hpp"

class HumanA {
    Weapon *weapon;
    std::string name;
public:
    HumanA(std::string name, Weapon &weapon);

    void attack();
};


#endif //CPP_HUMANA_HPP
