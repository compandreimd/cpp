/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex03.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 13:39:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 13:39:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>


int main() {
    std::string str = "HI THIS IS BRAIN";
    std::string &ref = str;
    std::string *point = &str;

    std::cout << "STR:" << str << std::endl;
    std::cout << "REF:" << ref << std::endl;
    std::cout << "  P:" << *point << std::endl;
}