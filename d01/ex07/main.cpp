/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amalcoci <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/19 15:25:00 by amalcoci          #+#    #+#             */
/*   Updated: 2017/07/19 15:25:00 by amalcoci         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>


static std::string replaceAll(std::string str, const std::string &from, const std::string &to) {
    size_t start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
    return str;
}

int main(int argc, char *argv[]) {
    if (argc == 4) {
        std::string name = std::string(argv[1]);
        std::ifstream in(name);
        std::ofstream out(name.append(".replace"));
        if (!in || !out) {
            std::cerr << "Error opening files!" << std::endl;
            return 1;
        }

        std::string tmp;
        std::getline(in, tmp);
        while (!in.eof()) {
            tmp = replaceAll(tmp, argv[2], argv[3]);
            out << tmp << std::endl;
            std::getline(in, tmp);
        }

        in.close();
        out.close();
    } else
        std::cout << "help: replace file s1 s2" << std::endl;
}
