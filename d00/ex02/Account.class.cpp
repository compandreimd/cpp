//
// Created by root on 7/17/17.
//


#include "Account.class.hpp"
#include <ctime>
#include <iostream>

/* Static instance intialization */

int Account::_nbAccounts = 0;
int Account::_totalAmount = 0;
int Account::_totalNbDeposits = 0;
int Account::_totalNbWithdrawals = 0;


/* Static methode */

int Account::getNbAccounts() {
    return Account::_nbAccounts;
}

int Account::getTotalAmount() {
    return Account::_totalAmount;
}

int Account::getNbDeposits() {
    return Account::_totalNbDeposits;
}

int Account::getNbWithdrawals() {
    return Account::_totalNbWithdrawals;
}

void Account::displayAccountsInfos() {
    _displayTimestamp();
    std::cout << "accounts:" << getNbAccounts()
              << ";total:" << getTotalAmount()
              << ";deposits:" << getNbDeposits()
              << ";withdrawals:" << getNbWithdrawals() << std::endl;
}

void Account::_displayTimestamp(void) {
    time_t theTime = time(NULL);
    struct tm *aTime = localtime(&theTime);
    char s[32];

    strftime(s, sizeof(s), "[%Y%m%d_%H%M%S] ", aTime);
    std::cout << s;
}

Account::Account(int initial_deposit) :
        _accountIndex(getNbAccounts()),
        _amount(initial_deposit),
        _nbDeposits(0),
        _nbWithdrawals(0) {
    _nbAccounts++;
    _totalAmount += _amount;
    _displayTimestamp();
    std::cout << "index:" << _accountIndex << ";amount:" << _amount << ";created" << std::endl;
}

Account::Account() :
        _accountIndex(getNbAccounts()),
        _amount(0),
        _nbDeposits(0),
        _nbWithdrawals(0) {
    _displayTimestamp();
    std::cout << "index:" << _accountIndex << ";amount:" << _amount << ";created" << std::endl;
}

Account::~Account() {
    _nbAccounts--;
    _displayTimestamp();
    std::cout << "index:" << _accountIndex << ";amount:" << _amount << ";closed" << std::endl;
    _totalAmount -= this->_amount;
}

void Account::makeDeposit(int deposit) {
    _displayTimestamp();
    _amount += deposit;
    _totalAmount += deposit;
    _nbDeposits++;
    _totalNbDeposits++;
    std::cout << "index:" << _accountIndex
              << ";p_amount:" << _amount - deposit
              << ";deposit:" << deposit
              << ";amount:" << _amount
              << ";nb_deposits:" << _nbDeposits << std::endl;
}

bool Account::makeWithdrawal(int withdrawal) {
    if (withdrawal > checkAmount()) {
        _displayTimestamp();
        std::cout << "index:" << _accountIndex
                  << ";p_amount:" << _amount
                  << ";withdrawal:refused" << std::endl;
        return false;
    }
    _displayTimestamp();
    _amount -= withdrawal;
    _totalAmount -= withdrawal;
    _nbWithdrawals++;
    _totalNbWithdrawals++;
    std::cout << "index:" << _accountIndex
              << ";p_amount:" << _amount + withdrawal
              << ";withdrawal:" << withdrawal
              << ";amount:" << _amount
              << ";nb_deposits:" << _nbDeposits << std::endl;

    return true;
}

int Account::checkAmount() const {
    static int i = 0;
    i++;
    _displayTimestamp();
    std::cout << "index:" << _accountIndex << ";amount:" << _amount << ";checks:" << i << std::endl;
    return _amount;
}

void Account::displayStatus() const {
    _displayTimestamp();
    std::cout << "index:" << _accountIndex
              << ";amount:" << _amount
              << ";deposits:" << _nbDeposits
              << ";withdrawals:" << _nbWithdrawals << std::endl;
}
