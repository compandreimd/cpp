#include <iostream>
#include "Contact.class.hpp"
#include <iomanip>

int main() {
    Contact contact[8];
    int last = 0;
    while (true) {
        std::cout << "Choose Action(ADD,SEARCH,EXIT):";
        std::string option;
        std::cin >> option;
        if (!option.compare("EXIT"))
            break;
        else if (!option.compare("ADD")) {
            if (last < 8) {
                contact[last].add();
                last++;
            } else
                std::cout << "MEMORY ERROR" << std::endl;
        } else if (!option.compare("SEARCH")) {
            std::cout << std::right << std::setw(10) << "Index" << " | ";
            std::cout << std::right << std::setw(10) << "First Name" << " | ";
            std::cout << std::right << std::setw(10) << "Last Name" << " | ";
            std::cout << std::right << std::setw(10) << "Nickname" << std::endl;
            for (int i = 0; i < last; i++)
                contact[i].show(i);
            std::cout << "SELECT:";
            unsigned i;
            std::cin >> i;
            if (i < (unsigned) last)
                contact[i].show_full();
            else
                std::cout << "ID ERROR" << std::endl;
        } else
            std::cout << "ACTION ERROR" << std::endl;

    }

    return 0;
}
