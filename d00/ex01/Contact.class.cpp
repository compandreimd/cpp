//
// Created by root on 7/17/17.
//

#include "Contact.class.hpp"
#include <iostream>
#include <iomanip>

std::string Contact::key(int id) const {
    switch (id) {
        case 0:
            return std::string("First Name");
        case 1:
            return std::string("Last Name");
        case 2:
            return std::string("Nickname");
        case 3:
            return std::string("Login");
        case 4:
            return std::string("Postal Adress");
        case 5:
            return std::string("Email Adress");
        case 6:
            return std::string("Phone Numbe");
        case 7:
            return std::string("Birthday Date");
        case 8:
            return std::string("Favorite Meal");
        case 9:
            return std::string("Underwear Color");
        case 10:
            return std::string("Darkest Secret");
    }
    return std::string("");
}

void Contact::add() {
    char tmp[255];
    std::cin.getline(tmp, 255);
    for (int i = 0; i < 11; i++) {
        std::cout << key(i) << ":";
        char line[255];
        std::cin.getline(line, 255);
        value[i] = std::string(line);
    }
}

void Contact::show(int id) const {
    std::cout << std::right << std::setw(10) << id << " | ";
    for (int i = 0; i < 2; i++)
        if (value[i].length() > 10)
            std::cout << std::right << std::setw(10) << value[i].substr(0, 9).append(".") << " | ";
        else
            std::cout << std::right << std::setw(10) << value[i].substr(0, 10) << " | ";
    if (value[2].length() > 10)
        std::cout << std::right << std::setw(10) << value[2].substr(0, 9).append(".") << std::endl;
    else
        std::cout << std::right << std::setw(10) << value[2].substr(0, 10) << std::endl;
}

void Contact::show_full() const {
    for (int i = 0; i < 11; i++) {
        std::cout << key(i) << ":" << value[i] << std::endl;
    }
}