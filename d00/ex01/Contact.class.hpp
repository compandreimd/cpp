//
// Created by root on 7/17/17.
//

#ifndef CPP_CONTACT_HPP
#define CPP_CONTACT_HPP

#include <string>

class Contact {
public:
    std::string value[11];

    std::string key(int id) const;

    void add();

    void show(int id) const;

    void show_full() const;
};


#endif //CPP_CONTACT_HPP
