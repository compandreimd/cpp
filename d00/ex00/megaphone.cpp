#include <iostream>

static char ft_toupper(char c) {
    if (c >= 'a' && c <= 'z')
        c -= 32;
    return (c);
}

static void ft_toup(char *str) {
    while (*str) {
        std::cout << ft_toupper(*str);
        str++;
    }

}

int main(int argc, char *argv[]) {
    if (argc == 1) {
        std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *"
                  << std::endl;
        return 0;
    }
    for (int i = 1; i < argc - 1; i++)
        ft_toup(argv[i]);

    ft_toup(argv[argc - 1]);
    std::cout << std::endl;
    return 0;
}
